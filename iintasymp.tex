\section{Asymptotic expansions}
\label{sec:asymptotic}
%
The last property of MPLs that we need is their
asymptotic expansions in various limits. We saw in \autoref{sec:algs} that
$G\brk{a_1, \ldots, a_n; z}$ has a convergent series expansion
when its arguments lie outside of the integration contour \eqref{eq:expanCond}:
$\abs{a_j} > \abs{z}$.
When this is not the case and some $\abs{a_i} < \abs{z}$ the expansion is no
longer convergent. To make the divergence of the function as $a_i
\to 0$ explicit an identity is needed that produces explicit powers of
$\log\brk{a_i}$ and $G$-functions with $a_i$ in the endpoint slot:
$G\brk{a_1, \ldots, a_n; z} \mapsto \brc{\log\brk{a_i}, G\brk{\ldots; a_i}}$.
In this subsection we describe how these identities can be generated in
several cases and give some examples that are used later in
\autoref{ch:spradlin}.

%
\begin{wrapfigure}[10]{r}[0pt]{3.7cm}
    \centering
    \includegraphicsbox{contourSplit}
    \caption{Contour decomposition used in \soft{HyperInt}. The branch here
    corresponds to the shift $a_i \to a_i - i \eps$}
    \label{fig:contourSplit}
\end{wrapfigure}
%
The following is not the only way to produce such expansions of MPLs. One
alternative is the algorithm of \soft{HyperInt} \cite{Panzer:2014caa}, implemented in \soft{Maple}.
To deal with the singularities arising from argument $a_i$ hitting
the contour of integration $\edge{z}{0}$ they split the path into two with an intermediate point $\tau$
%
\begin{align}
    \edge{z}{0} \mapsto \edge{z}{\tau} \edge{\tau}{0}\,,
\end{align}
%
and then convert the iterated integral along $\edge{\tau}{0}$ back to MPLs with
an appropriate M\"obius transform. The choice of the branch is then encoded in
the way $\edge{z}{\tau}$ goes above or below $a_i$ \brk{see
\autoref{fig:contourSplit}}.
%
Another way is to make use of the Hopf algebra of MPLs themselves
\cite{Duhr:2014woa}, which is something deeper than the shuffle algebra of
their arguments from \autoref{ssec:shuffleAlg}.

We trade the generality of the two above approaches for more
control and implement our version of the generator of such identities
based on Section 2.1.1 of \cite{Broedel:2014vla} and \cite{Vollinga:2004sn}.
Also we only consider patterns that appear in \autoref{ch:spradlin}.
%
\subsubsection{Identities for the logarithm}
%
Here we follow Section 4.2 of \cite{Vollinga:2004sn}.
Let us start with the simplest case of a logarithm. We take a small argument $s$
and an endpoint $z$ ordered as
%
\begin{align}
    0 < s < z\,,
    \label{eq:logExampleDomain}
\end{align}
%
and look for an identity for the logarithm
%
\begin{align}
    G\brk{s_{\pm}; z} = \log\Bigbrk{\frac{s_\pm - z}{s_\pm}}\,,
    \label{eq:logExample}
\end{align}
%
\begin{wrapfigure}[8]{r}[0pt]{0pt}
    \includegraphicsbox{contourBumpInv}
    \caption{Iterated integral representation of a MPL}
    \label{fig:contourInv}
\end{wrapfigure}
%
such that only functions $\brc{\log\brk{s}, G\brk{z; s}}$ appear on the RHS of
it. Note that an expression $G\brk{s; z}$ here would be ill-defined due to
\eqref{eq:logExampleDomain}, so we shift
$s \to s_{\pm} \defas s \pm i \eps$ which fixes the choice of the branch of
the RHS in \eqref{eq:logExample}. We interpret the sign of $\pm i \eps$ as a
choice of analytic continuation of a well-defined $G\brk{-s; z}
\xmapsto{\text{cont}} G\brk{s_{\pm}; z}$ \brk{see \autoref{fig:contourInv}}.
The desired rule for $G\brk{s_{\pm}; z}$ follows
from straightforward manipulations with logarithms:
%
\begin{align}
    G\brk{s_{\pm}; z}
    = \log\Bigbrk{\frac{z}{-s_{\pm}} \bigbrk{1 - \frac{s_{\pm}}{z}}}
    = - \log\brk{s} + \log\brk{-z_{\mp}} + G\brk{z; s}\,,
    \label{eq:logExampleId}
\end{align}
%
where:
%
\begin{itemize}
    \item $G\brk{z; s}$ is well-defined for \eqref{eq:logExampleDomain} and has
        a series expansion \eqref{eq:seriesLog}.
    \item $\log\brk{-z_{\mp}} = \log\brk{z} \pm i \pi$ encapsulates the choice
        of the branch on the RHS.
\end{itemize}
%
We see, if we let $s \to 0$, then \eqref{eq:logExampleId} makes the divergent
$\log\brk{s}$ explicit, which is one example of the decomposition
\eqref{eq:divLogsZero}.
%
\subsubsection{Identities for classical polylogarithms}
%
Here we follow Section 5.3 of \cite{Vollinga:2004sn}.
Let us increase the complexity a bit and consider the case of a classical
polylogarithm \brk{recall \eqref{eq:GtoLi}}:
%
\begin{align}
    G\brk{0^n, s_{\pm}; z} = -\Li_n\Bigbrk{\frac{z}{s_{\pm}}}\,,
    \label{eq:clpolyExample}
\end{align}
%
with the same setup for the arguments \eqref{eq:logExampleDomain}. In that case
we can derive a recursion in the weight $n$ that terminates at
\eqref{eq:logExampleId}:
%
\begin{align}
    G\brk{0^n, s_{\pm}; z}
    &= \int\limits_0^z \! \dd\log\brk{t_1} \, G\brk{0^{n - 1}, s_{\pm}; z}
    = G\brk{0^n, s_{\pm}; s} + \int\limits_s^z \! \dd\log\brk{t_1} \,
    G\brk{0^{n - 1}, s_{\pm}; t_1}
    \nn
    \\
    &= -\zeta_n + \int\limits_s^z \! \dd\log\brk{t} \, G\brk{0^{n - 1}, t_{\pm}; z}
    \nn
    \\
    &= -\zeta_n + \int\limits_0^z \! \dd\log\brk{t} \, G\brk{0^{n - 1}, t_{\pm}; z}
    - \int\limits_0^s \! \dd\log\brk{t} \, G\brk{0^{n - 1}, t_{\pm}; z}\,,
    \label{eq:clpolyId}
\end{align}
%
where:
%
\begin{itemize}
    \item $t = \frac{z s}{t_1}$ with $\dd\log\brk{t} = -\dd\log\brk{t_1}$ is
        introduced in the second line.
    \item On the RHS of \eqref{eq:clpolyId} second and third terms have reduced
        weight and the recursion continues through them since they are not of
        \eqref{eq:defG} form quite yet.
\end{itemize}

An example of \eqref{eq:clpolyId} applied to the dilogarithm:
%
\begin{align}
    G\brk{0, s_{\pm}; z}
    &= \frac{\bigbrk{\log\brk{s}}^2}{2}
    - \log\brk{s} \log\brk{-z_{\mp}}
    - \frac{\bigbrk{\log\brk{z}}^2}{2}
    + \log\brk{z} \log\brk{-z_{\mp}}
    - 2 \zeta_2
    - G\brk{0, z; s}
    \nn
    \\
    &= \frac{\pi^2}{6} + \Bigbrk{\log\bigbrk{\frac{s}{-z_\mp}}}^2
    - G\brk{0, z; s}\,.
    \label{eq:dilogExample}
\end{align}
%
Again we see that the choice of the branch $\pm i \eps$ is parametrized on the RHS
in $\log\brk{-z_{\mp}}$, which appears at the recursion termination point
\eqref{eq:logExampleId}.
%For the general case $G\brk{0^n, s_{\pm}; z}$ \eqref{eq:clpolyExample}
%the RHS of the identity generated by \eqref{eq:clpolyId} also consist of
%explicit powers of $\brc{\log\brk{s}^p}_{p \le n + 1}$ and analytic in $s$ 
%
\subsubsection{Identities for MPLs}
%
Here we follow Section 5.3 of \cite{Vollinga:2004sn}.
Consider a MPL with one smallest argument
%
\begin{align}
    0 < s < \brc{a_1, \ldots, a_n, z}
    \label{eq:mplIdPattDomain}
\end{align}
%
inside its list of arguments:
%
\begin{align}
    G\brk{a_1, \ldots, s_{\pm}, \ldots, a_n; z}\,,
    \label{eq:mplIdPatt}
\end{align}
%
and with the condition that at least $a_n \ne 0$, so that the expression is
meaningful \brk{otherwise we can perform the shuffle regularization}.
In order to bring $s$ to the endpoint slot of the MPLs we use the identity:
%
\begin{align}
    G\brk{a_1, \ldots, s_{\pm}, \ldots, a_n; z}
    = G\brk{a_1, \ldots, 0, \ldots, a_n; z}
    + \int\limits_0^s \! \dd t \> \partial_{t} G\brk{a_1, \ldots, t_{\pm}, \ldots,
    a_n; z} \label{eq:mplIdGen}\,.
\end{align}
%
The first term is free of $s$, so we don't touch it anymore.
The second term can be evaluated using the rules for derivatives
from \autoref{ssec:diff} to complete the recursion step.
Note that the algorithm branches
due to, for example, the 2 terms in \eqref{eq:diffGi} that still depend on the smallest argument $t$.
The recursion terminates when we either remove all the occurrences of $t$ or via \eqref{eq:logExampleId}.
In the latter case, we still need to reconstruct the MPLs from all the delayed integrations
\eqref{eq:mplIdGen}.

An example of generated identity for a weight 2 function:
%
\begin{align}
    G\brk{s_{\pm}, a; z}
    &= G\brk{0, a; z} - \log\brk{s} G\brk{a; s}
    + G\brk{a; z} \bigbrk{G\brk{z; s} - G\brk{a; s}}
    \nn
    \\
    &+ G\brk{0, a; s} + G\brk{a, z; s} + G\brk{a; s} \log\brk{-z_{\mp}}\,,
    \label{eq:mplIdExample}
\end{align}
%
where:
%
\begin{itemize}
    \item All arguments $0 < s < \brc{a, z}$ are real.
    \item There are no bare powers of $\log\brk{s}$ like in
        \eqref{eq:dilogExample} since the original
        function is well-defined in the limit $s \to 0$. The only occurence of
        $\log\brk{s}$ is together with an $G\brk{a; s} = -\frac{s}{a} + \order{s^2}$
        which removes the divergence.
\end{itemize}
%
Now we want to clarify the analytic structure of identities like
\eqref{eq:mplIdExample} with respect to $s$.
%
\subsubsection{Negative argument}
%
Another relevant case is when the smallest argument is understood as "negative" \brk{compare \eqref{eq:mplIdGen}}:
%
\begin{align}
    -s < 0 < \brc{a_1, \ldots, a_n, z}\,.
\end{align}
%
Then no $\pm i \eps$ regularization is needed on the LHS in
\eqref{eq:logExample}, \eqref{eq:clpolyExample}, and \eqref{eq:mplIdPatt}, so that the corresponding
identities can be analytically continued along the appropriate paths. On the
RHS the continuation only affects the explicit logarithms:
%
\begin{align}
    \log\brk{s} \to \log\brk{-s_{\mp}} = \log\brk{-s \pm i \eps} = \log\brk{s} \pm i \pi\,,
    \label{eq:log}
\end{align}
%
which is described by the deformation of the contour of integration discussed in
\autoref{ssec:disc}. The choice of $\pm i \eps$ is naturally derived from the initial regularization
and is summarized in \autoref{table:log}
%
\begin{table}
    \centering
\begin{tabular}{%
    p{5cm}|%
    >{\centering\arraybackslash}p{3.5cm}%
    >{\centering\arraybackslash}p{3.5cm}%
}
Analytic continuation path on the LHS of identity&
\includegraphicsbox{contourBumpBelow}
&
\includegraphicsbox{contourBumpAbove}
\\
Deformation of $\log\brk{s}$ contour on the RHS of identity
&
\includegraphicsbox{contourLeftAbove}
&
\includegraphicsbox{contourLeftBelow}
\\
\hline
Imaginary part of $\log\brk{s}$
&
$+ i \pi$
&
$- i \pi$
\end{tabular}
    \caption{The choice of the small imaginary shift $\pm i \eps$ of the
    argument of logarithm in \protect\eqref{eq:log} can be derived from the path of analytic
    continuation.
    }
    \label{table:log}
\end{table}
%
An example of this continuation procedure applied to the dilogarithm
\eqref{eq:dilogExample}:
%
\begin{align}
    G\brk{0, -s; z}
    = \frac{\pi^2}{6} + \Bigbrk{\log\bigbrk{\frac{s}{z}}}^2
    - G\brk{0, z; -s}\,.
    \label{eq:dilogExampleNeg}
\end{align}
%
As another example consider \eqref{eq:mplIdExample} and its continuation:
%
\begin{align}
    G\brk{-s, a; z}
    &= G\brk{0, a; z} - \log\brk{s} G\brk{a; -s}
    + G\brk{a; z} \bigbrk{G\brk{z; -s} - G\brk{a; -s}}
    \nn
    \\
    &+ G\brk{0, a; -s} + G\brk{a, z; -s} + G\brk{a; -s} \log\brk{z}\,,
    \label{eq:mplIdNegExample}
\end{align}
%
where:
%
\begin{itemize}
    \item All arguments $-s < 0 < \brc{a, z}$ are real.
    \item After the expansion of $\log\brk{-z_{\mp}}$ and $\log\brk{-s_{\mp}}$ all
        $\brk{i \pi}$'s cancel and we are left with a manifestly real expression.
    \item Remaining $G$-functions on the RHS are analytic at least for $s > 0$ as
        is the initial function on the LHS.
\end{itemize}
Finally, we give some additional examples that are used in \autoref{ch:spradlin} later.
%
\subsubsection{Taylor expansion}
%
Another application of \eqref{eq:mplIdGen} is the Taylor expansion of
$G\brk{\ldots, -\alpha + s, \ldots; z}$ with $\brc{z, \alpha} > 0$ as $s \to 0$.
Indeed, under these conditions, the initial function $G\brk{\ldots, -\alpha,
\ldots; z}$ is well-defined and no $\pm i \eps$ regularization is required. The
procedure then is the same as outlined above near \eqref{eq:mplIdGen}: use
formulas for derivatives from \autoref{ssec:diff} and recurse in the length of
list of arguments.
%
An example of such an identity for a dilogarithm:
%
\begin{align}
    G\brk{0, -a + s; z}
    = G\brk{0, -a; z}
    - G\brk{-a; z} G\brk{a; s} + G\brk{a, a; z} - G\brk{a, a + z; s}\,.
    \label{eq:dilogExampleLin}
\end{align}
%
And for a weight 2 function:
%
\begin{align}
    G\brk{-\alpha + s, a; z}
    &=
G\brk{-\alpha, a, z}
- G\brk{a, z} G\brk{a + \alpha, s}
+ G\brk{-\alpha, z} G\brk{a + \alpha, s}
\\
&+ G\brk{a, z} G\brk{\alpha + z, s}
- G\brk{a + \alpha, \alpha, s}
+ G\brk{a + \alpha, \alpha + z, s}\,.
\end{align}
%
Here all the MPLs can be directly expanded around $s = 0$ using
\eqref{eq:GtoLi} without any issues regarding the branch cut structure.
%
\subsubsection{Discussion}
%
In this section we showed one way of constructing the asymptotic expansions of MPLs. The concrete use cases presented
above are mostly tailored to the study of the seven gluon scattering in \autoref{ch:spradlin}. A more comprehensive \brk{and
also rather different} treatment of a similar problem can be found in \cite{Panzer:2014caa}.
%
% vim: fdm=syntax
