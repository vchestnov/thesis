\chapter{Supersymmetric Yang--Mills theories}
\label{ch:susy}
%
We start with a brief overview of the perturbative structure of the supersymmetric
Yang--Mills theories that we consider in \autoref{ch:beta}. We show the
Lagrangian that describes the class of the models in question and fix our
notation for the Feynman rules. Then in \autoref{ch:beta}, our goal is to
use this information to perturbatively compute the beta function of these
supersymmetric theories up to 3 loops to reproduce results of
\cite{Velizhanin:2008rw}.
%\motivation{
%    one of the considered theories, $\superN = 4$ supersymmetric Yang--Mills,
%    is believed to be integrable in the planar limit $\Nc \to \infty$, which
%    opens possibilities for a non-perturbative analysis of the physical
%    observables in that particular model.
%}
%
\section{The Lagrangian}
%
The entry point of our discussion is a family of Lagrangians implicitly parametrized by the number of supercharges
$\superN \in \brc{0, 1, 2, 4}$:
%
\begin{align}
    \Lagr\subrm{SYM}^{\superN} = \tr\Bigbrc{
        &-\frac12 F_{\mu \nu} F^{\mu \nu}
        + 2 i
        \bar{\lambda}_{\dot{\alpha}A}
        %\sigma_{\mu}^{\dot{\alpha}\beta} \covar^\mu
        \covar^{\dot{\alpha}\alpha}
        \lambda^A_\alpha
        - D_\mu \phi^{I} \covar^\mu \phi^{I}
        \nn
        \\
        &+ \frac12 g^2 \sbrk{\phi^{I}, \phi^{J}} \sbrk{\phi^{I}, \phi^{J}}
        - g
        \lambda^{\alpha A} \brk{\bar{\Sigma}^I}_{AB}
        \sbrk{\phi^I, \lambda^B_\alpha}
        + g
        \bar{\lambda}_{\dot{\alpha} A} \brk{\Sigma^I}^{AB}
        \sbrk{\phi^I, \bar{\lambda}^{\dot{\alpha}}_B}
    }\,,
    \label{eq:lagr}
\end{align}
%
where:
%
\begin{itemize}
    \item The 4-dimensional metric here is $\eta^{\mu\nu} = \diag\brk{\p, \m, \m, \m}$.
    \item
        $p^{\dot{\alpha} \alpha} = p^\mu \brk{\sigma_\mu}^{\dot{\alpha} \alpha}$ and
        $p_{\alpha \dot{\alpha}} = p^\mu \brk{\bar{\sigma}_\mu}_{\alpha \dot{\alpha}}$
        denote contraction of any 4-vector $p^\mu$ with the Pauli matrices $\sigma_\mu \defas \brc{1, \vec{\sigma}}$
        and $\bar{\sigma_\mu} \defas \brc{1, -\vec{\sigma}}$.
    \item $F_{\mu\nu} = \partial_\mu A_\nu - \partial_\nu A_\mu - i g \sbrk{A_\mu, A_\nu}$
        and $\covar_\mu = \partial_\mu - i g \sbrk{A_\mu, \bigcdot\mskip2mu}$.
    \item  $\brc{\alpha, \dot{\alpha}} = \brc{\brc{1, 2}, \brc{\dot{1}, \dot{2}}}$
        are the spinor indices.
    \item Collections of fermions and scalars are labeled by
        %
        \begin{align}
            A, B &\in \brc{1, \dots, n_f}\,,
            \label{eq:indRangeA}
            \\
            I &\in \brc{1, \dots, n_s}\,.
            \label{eq:indRangeI}
        \end{align}
        %
    \item All fields $\brc{A, \lambda, \phi}$ transform in the adjoint representation of the color group $SU\brk{\Nc}$ and
        $\Nc$ is often referred to as the number of colors.
    \item $\brc{\brk{\Sigma^I}^{AB}, \brk{\bar{\Sigma}^I}_{AB}}$ are the \brk{higher dimensional} Pauli matrices that parametrize
        the Yukawa coupling in the Lagrangian: $\Sigma^I = \brk{\bar{\Sigma}^I}^\ast$ and $\Sigma^I \bar{\Sigma}^J + \Sigma^J \bar{\Sigma}^I = -2 \delta^{IJ}$.
    \item $g$ is the coupling constant, which is related to the loop counting parameter $a$ and the strong coupling constant $\alpha_s$ via:
        %
        \begin{align}
            a = \frac{\alpha_s}{4 \pi} = \frac{g^2}{\brk{4 \pi}^2}\,.
            \label{eq:coupConst}
        \end{align}
        %
    \item We omit the ghost and gauge fixing terms.
    \item In the $\superN = 1$ case the $\phi$-terms are absent and in the $\superN = 0$ the $\lambda$'s disappear as well.
\end{itemize}
%
The Lagrangian in \eqref{eq:lagr} can be constructed using the dimensional reduction of the $\superN = 1$ SYM in $\dimredD$ dimensions
\cite{Gliozzi:1976qd,Brink:1976bc}.
Here we follow a slightly more modern notation described in: Chapter~IV~\cite{Dorey:2002ik}, Appendix A \cite{Belitsky:2003sh},
as well as in: Chapter 1 \cite{Groeger:2012hqk}, Appendix C \cite{Wiegandt:2012zna, Wiegandt:2012eu}.
The family of supersymmetric theories has the following field content: gauge field $A_\mu$,
$n_s$ real scalars $\phi^I$, $n_f$ complex Weyl fermions $\brc{\lambda^{\alpha A}, \bar{\lambda}^{\dot{\alpha} A}}$
with conjugation property $\brk{\lambda^{\alpha A}}^\ast = \bar{\lambda}^{\dot{\alpha}A}$.
It interpolates between supersymmetric Yang--Mills theories with different number of supercharges
$\superN$ via the different choice of the number of fermions
$n_f$ and scalars $n_s$ \cite{Belitsky:2003sh, Velizhanin:2008rw, Grozin:2015kna}:
%
\begin{table}[h]
    \centering
    \begin{tabular}{c c | c c}
        $\dimredD$ & $\superN$ & $n_f$ & $n_s$ \\
        \hline
        10 & 4 & 4 & 6 \\
        6 & 2 & 2 & 2 \\
        4 & 1 & 1 & 0 \\
        \hline
        & 0 & 0 & 0
    \end{tabular}
    \caption{Supersymmetric Yang--Mills \brk{SYM} theories with various number of supercharges $\superN$
        can be obtained by dimensional reduction to 4 dimensions of $\superN = 1$ \brk{SYM} in $\dimredD$ dimensions.
        In addition to the usual gauge bosons, these theories have different numbers of fermions $n_f$ and scalars $n_s$.
        Here $\superN = 4$ is the so-called maximally supersymmetric Yang--Mills
        theory and $\superN = 0$ represents the pure Yang--Mills theory \brk{or QCD
        without quarks}.}
    \label{table:nsnf}
\end{table}

As an illustration, let us discuss the scalar field $\phi^I$ in case of the $\superN = 4$ theory in more detail
\brk{we follow the notation of \cite{Belitsky:2003sh}}.
Pauli matrices, that describe the fermion-scalar interaction, can then be represented in terms of the t'Hooft symbols
$\brc{\eta_{iAB}, \bar{\eta}_{iAB}}$ \cite{tHooft:1976snw}:
%
\begin{align}
    \brc{\brk{\Sigma^1}^{AB}, \ldots, \brk{\Sigma^6}^{AB}}
    & \defas \brc{\eta_{1AB}, \eta_{2AB}, \eta_{3AB}, i \bar{\eta}_{1AB}, i \bar{\eta}_{2AB}, i \bar{\eta}_{3AB}}\,,
    \\
    \brc{\brk{\bar{\Sigma}^1}_{AB}, \ldots, \brk{\bar{\Sigma}^6}_{AB}}
    & \defas \brc{\eta_{1AB}, \eta_{2AB}, \eta_{3AB}, -i \bar{\eta}_{1AB}, -i \bar{\eta}_{2AB}, -i \bar{\eta}_{3AB}}\,,
\end{align}
%
where:
%
\begin{align}
    \eta_{iAB} &\defas \eps_{iAB} + \delta_{iA} \delta_{4B} - \delta_{iB} \delta_{4A}\,,
    \\
    \bar{\eta}_{iAB} &\defas \eps_{iAB} - \delta_{iA} \delta_{4B} + \delta_{iB} \delta_{4A}\,,
\end{align}
%
together with the condition for the antisymmetric symbol $\eps_{iAB}$: $\eps_{i4B} = \eps_{iA4} = 0$. We can further
repack the 6 scalar fields $\phi^I$ into 3 complex combinations $\phi^{AB}$ using
%
\begin{align}
    \phi^{AB} &\defas \frac{1}{\sqrt{2}} \brk{\Sigma^I}^{AB} \phi^I\,,
    \\
    \bar{\phi}_{AB} &\defas \brk{\phi^{AB}}^\ast = \frac{1}{\sqrt{2}} \brk{\bar{\Sigma}^I}_{AB} \phi^I\,,
\end{align}
%
and more explicitly 
%
\begin{align}
    \phi^{AB} = \frac{1}{\sqrt{2}}
    \begin{pmatrix}
        0 & \phi^3 + i \phi^6 & -\phi^2 - i \phi^5 & \phi^1 - i \phi^4
        \\
        & 0 & \phi^1 + i \phi^4 & \phi^2 - i \phi^5
        \\
        & & 0 & \phi^3 - i \phi^6
        \\
        & & & 0
    \end{pmatrix}\,,
\end{align}
%
where we omitted the lower antisymmetric half of the matrix. In particular, the scalar product of two real vectors
$\brc{X^I, Y^J}$ is related to the trace of $\brc{X^{AB}, \bar{Y}_{CD}}$ as \brk{note the order of indices and
see also Chapter 1 in \cite{Groeger:2012hqk} for more details}:
%
\begin{align}
    X^{AB} \bar{Y}_{AB} = \frac12 \brk{\Sigma^I}^{AB} \brk{\bar{\Sigma}^J}_{AB} X^I Y^J = 2 X^I Y^I = \bar{X}_{AB} Y^{AB}\,.
\end{align}
%
Another popular choice of parametrization is
$\brc{Z, W, X} \defas \frac{1}{\sqrt{2}} \brc{\phi^1 + i \phi^2, \phi^3 + i \phi^4, \phi^5 + i \phi^6}$ \cite{Minahan:2010js, Beisert:2010jr}.
In the case of the $\superN = 2$ theory, $\brc{\brk{\Sigma^I}^{AB}, \brk{\bar{\Sigma}^I}_{AB}}$
can be defined in terms of the second Pauli matrix $\sigma_2$ \cite{Dorey:2002ik}:
%
\begin{align}
    \brc{\brk{\Sigma^1}^{AB}, \brk{\Sigma^2}^{AB}} \defas \brc{-\sigma_2, i \sigma_2}\,,
\end{align}
%
and similar complex conjugated version for $\brk{\bar{\Sigma}^I}_{AB}$.
%
\section{Feynman rules}
\label{sec:feynmanRules}
%
A very thorough derivation of the Feynman rules for the maximally
supersymmetric Yang--Mills theory in position space can be found in the thesis
\cite{Groeger:2012hqk} \brk{see also \cite{Wiegandt:2012zna, Wiegandt:2012eu}}.
Propagators and interaction vertices of gluons and ghosts remain
unchanged for all $\superN \in \brc{0, 1, 2, 4}$.
Therefore, we only discuss additional propagators
and verticies that involve fermions and scalars introduced by the
supersymmetry in \eqref{eq:lagr} with emphasis on the spinor and $R$-symmetry
structures. In the following, the generators $T^a$ of the color
$\mathfrak{su}\brk{\Nc}$ algebra are normalized like that: $\sbrk{T^a, T^b} = i f^{abc} T^c$ and $\tr\brk{T^a T^b} = \frac12 \delta^{ab}$.
%
\def\axobase{10}
\def\axoarrowscale{.8}
\def\axovertex(#1){
    \begin{axopicture}(90,60)
        %\AxoGrid(0,0)(10,10)(9,6){LightGray}{0.5}

        #1(45,30)(45,60){2}{4}
        \Line[arrow](30,\axobase)(45,30)
        \Line[arrow](45,30)(60,\axobase)
        \Vertex(45,30){2}
        \Text(50,60)[lt]{$\mu$}
        \Text(30,\axobase)[rc]{$\brc{A, \alpha}$}
        \Text(60,\axobase)[lc]{$\brc{B, \dot{\alpha}}$}
    \end{axopicture}
}
%
\subsection{Propagators}
%
Fermion and scalar propagators originate from the two quadratic terms in the Lagrangian
\eqref{eq:lagr}:
%
\begin{align}
    \Bigbrc{
        i \bar{\lambda}^a_{\dot{\alpha}A}
        \partial^{\dot{\alpha}\alpha}
        \lambda^{aA}_\alpha, \,
        -\frac12 \partial_\mu \phi^{aI} \partial^\mu \phi^{aI}
    }\,.
\end{align}
%
Inverting the corresponding quadratic operators we obtain the propagators in momentum space:
%
\begin{eqnarray}
    % fermion propagator
    \raisebox{-7.1pt}{
    \begin{axopicture}(120,20)(-10,0)
        %%\AxoGrid(-10,0)(10,10)(12,2){LightGray}{0.5}

        \Line[arrow](35,10)(65,10)
        %\Vertex(35,10){2}
        %\Vertex(65,10){2}
        \Text(30,10)[rc]{$\brc{a, A, \dot{\alpha}}$}
        \Text(70,10)[lc]{$\brc{b, B, \beta}$}
        \Red{
            \Text(50,6)[tc]{$p$}
        }
    \end{axopicture}
    }
    %
    &= &i \delta^{ab} \delta^B_A \frac{p_{\beta \dot{\alpha}}}{p^2}\,,
    \label{eq:fermionPropLagr}
    \\
    % scalar propagator
    \raisebox{-7.1pt}{
    \begin{axopicture}(120,20)(-10,0)
        %%\AxoGrid(0,0)(10,10)(10,2){LightGray}{0.5}

        \Line[dash](35,10)(65,10)
        %\Vertex(35,10){2}
        %\Vertex(65,10){2}
        \Text(30,10)[rc]{$\brc{a, I}$}
        \Text(70,10)[lc]{$\brc{b, J}$}
        \Red{
            \Text(50,6)[tc]{$p$}
        }
    \end{axopicture}
    }
    %
    &= &i \delta^{ab} \delta^{IJ} \frac{1}{p^2}\,.
    \label{eq:scalarProp}
\end{eqnarray}
%
\subsection{3-vertices}
\label{ssec:3vertices}
%
The 3 point vertices are generated by the interaction terms of \eqref{eq:lagr}
that are proportional to the coupling constant $g$:
%
\begin{align}
    \Bigbrc{
        \sbrk{A_\mu, \phi^{I}} \partial^\mu \phi^{I}, \,
        \bar{\lambda}_{\dot{\alpha}A}
        \sbrk{A^{\dot{\alpha}\alpha}, \lambda^A_\alpha}, \,
        \lambda^{\alpha A} \brk{\Sigma^I}^{AB}
        \sbrk{\phi^I, \lambda^B_\alpha}, \,
        \bar{\lambda}_{\dot{\alpha} A} \brk{\bar{\Sigma}^I}_{AB}
        \sbrk{\phi^I, \bar{\lambda}^{\dot{\alpha}}_B}
    }\,.
\end{align}
%
After symmetrization over the fields of the same type, these objects give rise
to the interaction vertices \brk{up to overall factors of $i$ and $-1$}:
%
\begin{equation}
    \begin{aligned}
    \raisebox{-27.1pt}{
        % ssg vertex
        \begin{axopicture}(110,60)(-10,0)
            % %\AxoGrid(-10,0)(10,10)(11,6){LightGray}{0.5}

            \Gluon(45,30)(45,60){2}{4}
            \Line[dash](30,\axobase)(45,30)
            \Line[dash](45,30)(60,\axobase)

            \Vertex(45,30){2}
            \Text(50,60)[lt]{$\brc{c, \mu}$}
            \Text(30,\axobase)[rc]{$\brc{a, I}$}
            \Text(60,\axobase)[lc]{$\brc{b, J}$}

            \Red{
                \Line[arrow,arrowpos=1,arrowinset=0](30.5, 19.)(36.5, 27.)
                \Line[arrow,arrowpos=1,arrowinset=0](59.5, 19.)(53.5, 27.)
                \Text(31.5, 24.5)[rb]{$p_2$}
                \Text(58.5, 24.5)[lb]{$p_3$}
            }
        \end{axopicture}
    }
    &\propto f^{abc} \delta_{IJ} \brk{p_3^\mu - p_2^\mu}\,,
    \\
    \raisebox{-27.1pt}{
        % afg vertex
        \begin{axopicture}(110,60)(-10,0)
            % %\AxoGrid(0,0)(10,10)(20,6){LightGray}{0.5}

            \Gluon(45,30)(45,60){2}{4}
            \Line[arrow](30,\axobase)(45,30)
            \Line[arrow](45,30)(60,\axobase)
            \Vertex(45,30){2}
            \Text(50,60)[lt]{$\brc{c, \mu}$}
            \Text(30,\axobase)[rc]{$\brc{a, A, \alpha}$}
            \Text(60,\axobase)[lc]{$\brc{b, B, \dot{\beta}}$}
        \end{axopicture}
    }
    &\propto f^{abc} \delta^B_A \brk{\sigma_\mu}^{\dot{\beta} \alpha}\,,
    %\label{eq:afgVertexLagr}
    \end{aligned}
    \qquad
    \begin{aligned}
    \raisebox{-27.1pt}{
        % ffs vertex
        \begin{axopicture}(110,60)(-10,0)
            % %\AxoGrid(0,0)(10,10)(20,6){LightGray}{0.5}

            \Line[dash](45,30)(45,60)
            \Line[arrow](30,\axobase)(45,30)
            \Line[arrow](60,\axobase)(45,30)
            \Vertex(45,30){2}
            \Text(50,60)[lt]{$\brc{c, I}$}
            \Text(30,\axobase)[rc]{$\brc{a, A, \alpha}$}
            \Text(60,\axobase)[lc]{$\brc{b, B, \beta}$}
        \end{axopicture}
    }
    &\propto f^{abc} \brk{\Sigma^I}^{AB} \eps^{\alpha \beta}\,,
    %\label{eq:ffsVertex}
    \\
    \raisebox{-27.1pt}{
        % aas vertex
        \begin{axopicture}(110,60)(-10,0)
            % %\AxoGrid(0,0)(10,10)(20,6){LightGray}{0.5}

            \Line[dash](45,30)(45,60)
            \Line[arrow](45,30)(30,\axobase)
            \Line[arrow](45,30)(60,\axobase)
            \Vertex(45,30){2}
            \Text(50,60)[lt]{$\brc{c, I}$}
            \Text(30,\axobase)[rc]{$\brc{a, A, \dot{\alpha}}$}
            \Text(60,\axobase)[lc]{$\brc{b, B, \dot{\beta}}$}
        \end{axopicture}
    }
    &\propto f^{abc} \brk{\bar{\Sigma}^I}_{AB} \eps^{\dot{\alpha} \dot{\beta}}\,.
    %\label{eq:aasVertex}
    \end{aligned}
    \label{eq:3vertices}
\end{equation}
%
\subsection{4-vertices}
\label{ssec:4vertices}
%
The vertices of degree 4 are produced by the $g^2$-terms in the Lagrangian
\eqref{eq:lagr}:
%
\begin{align}
    \Bigbrc{
        \sbrk{A_\mu \phi^{I}} \sbrk{A^\mu \phi^{I}}, \,
        \sbrk{\phi^{I}, \phi^{J}} \sbrk{\phi^{I}, \phi^{J}}
    }\,.
\end{align}
%
Again we need to add all possible permutations in order to generate the Feynman rules and,
omitting factors of $i$, we get the tensorial structures:
%
\begin{eqnarray}
    \raisebox{-27.1pt}{
        % ggss vertex
        \begin{axopicture}(90,60)
            %\AxoGrid(0,0)(10,10)(20,6){LightGray}{0.5}

            \Gluon(30,50)(45,30){-2}{4}
            \Gluon(60,50)(45,30){2}{4}
            \Line[dash](30,\axobase)(45,30)
            \Line[dash](60,\axobase)(45,30)
            \Vertex(45,30){2}
            \Text(30,50)[rc]{$\brc{a_4, \mu}$}
            \Text(60,50)[lc]{$\brc{a_3, \nu}$}
            \Text(30,\axobase)[rc]{$\brc{a_1, I}$}
            \Text(60,\axobase)[lc]{$\brc{a_2, J}$}
        \end{axopicture}
    }
    &\propto &\delta_{IJ} \eta^{\mu \nu} \brk{
        f^{a_1 a_3 a} f^{a_2 a_4 a} + f^{a_1 a_4 a} f^{a_2 a_3 a}
    }\,,
    \\
    \raisebox{-27.1pt}{
        % ssss vertex
        \begin{axopicture}(90,60)
            %\AxoGrid(0,0)(10,10)(20,6){LightGray}{0.5}

            \Line[dash](30,50)(45,30)
            \Line[dash](60,50)(45,30)
            \Line[dash](30,\axobase)(45,30)
            \Line[dash](60,\axobase)(45,30)
            \Vertex(45,30){2}
            \Text(30,50)[rc]{$\brc{a_4, I_4}$}
            \Text(60,50)[lc]{$\brc{a_3, I_3}$}
            \Text(30,\axobase)[rc]{$\brc{a_1, I_1}$}
            \Text(60,\axobase)[lc]{$\brc{a_2, I_2}$}
        \end{axopicture}
    }
    &\propto &\brk{
        \delta_{I_1 I_3} \delta_{I_2 I_4} - \delta_{I_1 I_4} \delta_{I_2 I_3}
    } f^{a_1 a_2 a} f^{a_3 a_4 a} + \brk{2 \leftrightarrow 4, 3}\,.
\end{eqnarray}
%
\subsection{Example of integrand}
\label{sec:example}
%
Let us now concentrate on the numerators that the above Feynman
rules produce. Since all the fields of the Lagrangian \eqref{eq:lagr} are in
the adjoint representation of the color group $SU\brk{\Nc}$, the corresponding
factors are quite similar to the pure Yang--Mills theory, so we postpone this
discussion until \autoref{ch:beta}. To see what distinguishes $\superN
\in \brc{1, 2, 4}$ theories we look at the fermion propagators and vertices.
Then there are two types of decorations \brk{apart from the color}:
%
\begin{itemize}
    \item One produces the usual traces of spinor matrices $\brk{\sigma_\mu}^{\dot{\beta}\alpha}$\,.
    \item The other generates traces of the $R$-symmetry matrices $\brk{\Sigma^I}^{AB}$\,.
\end{itemize}
%
As an example, consider the integrand of the fermion self-energy
that consists of two Feynman diagrams:
%
\def\axobase{20}
%
\begin{eqnarray}
    \raisebox{-17.1pt}{
        \begin{axopicture}(180,60)
            %%\AxoGrid(0,0)(10,10)(20,6){LightGray}{0.5}

            \Line[arrow](40,\axobase)(70,\axobase)
            \Line[arrow](70,\axobase)(110,\axobase)
            \Line[arrow](110,\axobase)(140,\axobase)
            \GluonArc(90,\axobase)(20,0,180){2}{10}
            \Vertex(70,\axobase){2}
            \Vertex(110,\axobase){2}
            \Text(40,\axobase)[rc]{$\brc{a, A, {\alpha}}$}
            \Text(140,\axobase)[lc]{$\brc{b, B, \dot{\beta}}$}
            \Red{
                \Text(90,15)[ct]{$p_1$}
                \Text(90,45)[cb]{$p_2$}
            }
            %\Text(55,25)[cb]{$Q$}
            \Text(70,35)[rt]{$\mu$}
            \Text(110,35)[lt]{$\nu$}
        \end{axopicture}
    }
    &\propto
    &\brk{\sigma_\nu \bar{\sigma}_{\mu_1} \sigma_\mu}^{\dot{\beta} \alpha}
    \delta_{AB} \times \text{gluon}^{\mu \nu} \frac{p_1^{\mu_1}}{p_1^2}\,,
    \\
    \raisebox{-17.1pt}{
        \begin{axopicture}(180,60)
            %%\AxoGrid(0,0)(10,10)(20,6){LightGray}{0.5}

            \Line[arrow](40,\axobase)(70,\axobase)
            \Line[arrow](110,\axobase)(70,\axobase)
            \Line[arrow](110,\axobase)(140,\axobase)
            \Arc[dash](90,\axobase)(20,0,180)
            \Vertex(70,\axobase){2}
            \Vertex(110,\axobase){2}
            \Text(40,\axobase)[rc]{$\brc{a, A, {\alpha}}$}
            \Text(140,\axobase)[lc]{$\brc{b, B, \dot{\beta}}$}
            \Red{
                \Text(90,15)[ct]{$p_1$}
                \Text(90,45)[cb]{$p_2$}
            }
            %\Text(55,25)[cb]{$Q$}
            \Text(70,35)[rt]{$I$}
            \Text(110,35)[lt]{$J$}
        \end{axopicture}
    }
    &\propto
    &n_s \brk{\sigma_{\mu_1}}^{\dot{\beta} \alpha} \delta_{AB}
    \times
    \frac{1}{p_2^2}
    \frac{p_1^{\mu_1}}{p_1^2}\,,
\end{eqnarray}
%
where \brk{see \autoref{ch:beta} for details}:
%
\begin{itemize}
    \item We omitted the common color factor of $C_A \delta^{ab}$\,.
    \item $\text{gluon}^{\mu \nu}$ is the gluon propagator \eqref{eq:gluonFull}\,.
    \item We used $\brk{\Sigma^I \bar{\Sigma}^I}^B_A \propto n_s \delta^B_A$\,.
\end{itemize}
%
We see that the spinorial structure gets contributions from all vertices
\eqref{eq:3vertices} and propagators \eqref{eq:fermionPropLagr}, but the
$R$-symmetry factor is constructed only from the Yukawa vertices in the second
column of \eqref{eq:3vertices}. Note, that in order to evaluate the $R$-symmetry
traces we can still use the usual rules for traces of gamma matrices in Euclidean
\brk{symbolic} $n_s$ dimensions.

\begin{wrapfigure}[16]{r}[0pt]{.25\textwidth}
    \centering
%\fbox%
{
    \begin{axopicture}(80,70)(0,0)
        \input{./axo/dis2}
    \end{axopicture}
}
    \caption{Electron-hadron $e^-h$ deep-inelastic scattering via a virtual photon exchange with space-like momentum $-q^2 > 0$.
    The grey vertex represents the internal structure of the hadron $h$, which is described by the space-like structure
    functions $\structureS{i}$. $X$ stands for the produced hadronic state.}
    \label{fig:dis2}
\end{wrapfigure}

In \autoref{ch:beta} we describe the renormalization of the supersymmetric
Yang--Mills theory \eqref{eq:lagr} up to 3 loops and compute similar
self-energy diagrams for other fields as well as some 3 point vertices, which we do with the help of computer algebra.

\section{Possible application to QCD}
%
Let us close this chapter with some comments about the relevance of supersymmetric theories
described by the Lagrangian \eqref{eq:lagr} to QCD and collider physics.

One point of contact appears in the perturbative analysis of splitting functions that 
govern the scale dependence of the proton's parton distribution functions \brk{PDFs} in QCD \cite{Moch:2017uml},
which is an essential part of the quantitative description of collision processes involving hadrons,
in particular, the electron-hadron deep-inelastic scattering \brk{DIS} depicted in \autoref{fig:dis2}
\cite{Tanabashi:2018oca}.
At the leading order in the QED coupling constant $\alpha_e$ only one virtual photon with a space-like
momentum $-q^2 > 0$
is exchanged, so that the \brk{differential} cross section $\sigma\brk{e^- + h \to e^- + X}$
can be decomposed into two Lorentz factors: lepton tensor $L_{\mu\nu}$, which describes the coupling of the
photon and the electron $e^-$, and hadron tensor $H_{\mu\nu}$ associated with the target hadron $h$ and produced
hadron state $X$.
%
The hadron tensor $H_{\mu\nu}$ is then further decomposed into proton space-like structure functions $\structureS{i}\brk{x, -q^2}$
%
\begin{align}
    H_{\mu\nu} &= \Bigbrk{-\eta_{\mu\nu} + \frac{q_\mu q_\nu}{q^2}} \structureS{1}\brk{x, -q^2}
    + \Bigbrk{p_\mu - \frac{p \cdot q}{q^2} q_\mu}
    \Bigbrk{p_\nu - \frac{p \cdot q}{q^2} q_\nu} \frac{1}{p \cdot q} \structureS{2}\brk{x, -q^2}
    \nn\\
    &- i \eps_{\mu\nu\alpha\beta} q^\alpha p^\beta \frac{1}{2 p \cdot q} \structureS{3}\brk{x, -q^2}\,,
\end{align}
%
where:
%
\begin{itemize}
    \item $q_\mu$ is the transferred momentum \brk{see \autoref{fig:dis2}} of the photon.
    \item $p_\mu$ is the momentum of the target hadron.
    \item $x$ is the Bjorken scaling variable:
        \begin{align}
            x = \frac{-q^2}{2 p \cdot q}\,,
            \label{eq:bjorkenS}
        \end{align}
        with the physical kinematical region located at $0 < x < 1$.
\end{itemize}

Within the parton model of hadrons, PDF $\pdf{a}\brk{x, \mu^2}$ measures the probability of a parton $a$ \brk{quark, antiquark,
gluon} having a momentum fraction $x$ inside of the proton:
%
\begin{align}
    \structureS{i}\brk{x, -q^2}
    = \int\limits_x^1 \dd{\log\brk{z}} \mskip3mu \coeffS{i, a}\Bigbrk{\frac{x}{z}, \alpha_s\brk{\mu^2}}
    \mskip3mu \pdf{a}\brk{z, \mu^2} 
    + \order{q^{-2}}
    \equiv \sbrk{\coeffS{i, a} \otimes \pdf{a}}\brk{x}
    + \order{q^{-2}}\,,
\end{align}
%
where:
%
\begin{itemize}
    \item $\otimes$ traditionally denotes the Mellin convolution:
%
\begin{align}
    \sbrk{f \otimes g}\brk{x} \defas
    \mskip10mu \mathclap{\int\limits_{\sbrk{0, 1} \times \sbrk{0, 1}}}
    \mskip10mu \dd x_1 \mskip1mu \dd x_2 \mskip3mu
    f\brk{x_1} g\brk{x_2} \delta\brk{x - x_1 x_2}\,,
\end{align}
%
    \item The coefficient functions $\coeffS{i, a}$ admit a perturbative expansion in the strong coupling $\alpha_s$\,.
    \item $\order{q^{-2}}$ denotes the $q^2$-suppressed terms with their own non-perturbative factors\,.
    \item $\mu$ is the renormalization scale\,.
\end{itemize}
%
Being a low energy non-perturbative quantity, $\pdf{a}\brk{x, \mu^2}$ has to be first determined experimentally at some initial
scale $\mu_0^2$ and then transferred to some other renormalization scale $\mu^2$ of interest using the DGLAP evolution equation
\cite{Gribov:1972ri, Dokshitzer:1977sg, Altarelli:1977zs}:
%
\begin{align}
    \frac{\dd \pdf{a}\brk{x, \mu^2}}{\dd{\log\brk{\mu^2}}}
    = \sbrk{\splitting{ab}^- \otimes \pdf{b}}\brk{x}\,,
\end{align}
%
where the distributions $\splitting{ab}^-$ are called the splitting functions.
%and can be calculated in perturbation theory:
%\cite{Buza:1996wv}
%%
%\begin{align}
%    \splitting{ab}^-\brk{x, \alpha_s\brk{\mu^2}}
%    = \alpha_s \splitting{ab}^{-, {0}}\brk{x}
%    + \alpha_s^2 \splitting{ab}^{-, {1}}\brk{x}
%    + \alpha_s^3 \splitting{ab}^{-, {2}}\brk{x}
%    + \ldots
%\end{align}
%%
As a consequence of the Operator Product Expansion, $\splitting{ab}^-$ are related to anomalous dimensions $\anomdim{ab}$
of twist-2 local Wilson operators $\Bra{b} \oper^a \Ket{b}$ via a Mellin transformation: 
%
\begin{align}
    \anomdim{ab}^-\brk{N, \alpha_s} = - \int\limits_0^1 \dd{\log\brk{x}} \mskip3mu x^N \splitting{ab}^-\brk{x, \alpha_s}\,.
    \label{eq:anomdimS}
\end{align}
%
Consider the following local Wilson twist-2 operators \cite{Kotikov:2001sc, Kotikov:2003fb, Kotikov:2004er, Kotikov:2005ne}
%
\begin{align}
    \oper^g_{\mu_1, \ldots, \mu_N} &= \symm\sbrk{F_{\mu \mu_1}^a \covar_{\mu_2} \ldots \covar_{\mu_{N - 1}} F_{\mu \mu_N}^a}\,,
    \\
    \oper^{\lambda}_{\mu_1, \ldots, \mu_N} &= \symm\sbrk{\bar{\psi}^{a}_A \gamma_{\mu_1} \covar_{\mu_2} \ldots \covar_{\mu_{N}} \psi^{aA}}\,,
    \\
    \oper^{\phi}_{\mu_1, \ldots, \mu_N} &= \symm\sbrk{\bar{\phi}^a_{AB} \covar_{\mu_1} \ldots \covar_{\mu_{N}} \phi^{aAB}}\,,
\end{align}
%
where:
%
\begin{itemize}
    \item The symbol $\symm$ stands for symmetrization of the Lorentz indices $\brc{\mu_1, \ldots, \mu_N}$ and subtraction of
        traces.
    \item The 4-component spinors $\psi^{aA}$ are formed out of Weyl spinors $\brc{\lambda, \bar{\lambda}}$ using
$\psi^{aA} = \begin{pmatrix} \lambda^{aA}_{\alpha} \\ \bar{\lambda}^a_{\dot{\alpha}A} \end{pmatrix}$.
\end{itemize}

\begin{wrapfigure}[14]{r}[0pt]{.25\textwidth}
    \centering
%\fbox%
{
    \begin{axopicture}(90,70)(0,0)
        \input{./axo/dis1}
    \end{axopicture}
}
    \caption{Hadron production in electron-positron $e^+e^-$ annihilation mediated by an exchange of photon with
    time-like momentum $\brk{+q^2 > 0}$ and described by the time-like fragmentation functions $\structureT{j}$
    }
    \label{fig:dis1}
\end{wrapfigure}
%
Finally, we briefly list the analytic results that exist for the twist-2 anomalous dimensions:
at 3 loops in \cite{Vogt:2004mw, Moch:2004pa}, at 4 loops in the planar $\Nc \to \infty$ limit in \cite{Moch:2017uml}
and up to $N = 3$ at 5 loops in \cite{Herzog:2018kwj}. In the case of $\superN = 4$ SYM, similar information is available:
at 3 loops \cite{Kotikov:2003fb, Kotikov:2004er, Kotikov:2005ne} 
and partial results at 4 loops \cite{Velizhanin:2008pc, Velizhanin:2009gv, Velizhanin:2010ey, Velizhanin:2014zla} up to
spin $N = 8$.
%in QCD at 4 loops up to $N = 4$ in \cite{Velizhanin:2011es, Velizhanin:2014fua}

Another scattering process related to DIS by crossing symmetry is the $e^+e^-$ annihilation shown in \autoref{fig:dis1}.
At the lowest order in QED corrections, it is described by one exchange photon with a time-like momentum $q^2 > 0$.
Similarly to DIS, the cross-section $\sigma\brk{e^- + e^+ \to h + X}$ is decomposed into leptonic and hadronic $H_{\mu\nu}$ tensors
and the latter can be written as: 
%
\begin{align}
    H_{\mu\nu} &= \Bigbrk{-\eta_{\mu\nu} + \frac{p_\mu q_\nu + p_\nu q_\mu}{p \cdot q} - \frac{q^2}{\brk{p \cdot q}^2} p_\mu p_\nu}
    \frac{q^2}{2 p \cdot q} \structureT{T}\brk{q^2, z}
    \nn\\
    &+ \Bigbrk{p_\mu - \frac{p \cdot q}{q^2} q_\mu}
    \Bigbrk{p_\nu - \frac{p \cdot q}{q^2} q_\nu} \frac{q^4}{\brk{p \cdot q}^3} \structureT{L}\brk{z, q^2}
    \nn\\
    &- i \eps_{\mu\nu\alpha\beta} q^\alpha p^\beta \frac{q^2}{2 p \cdot q} \structureT{A}\brk{z, q^2},
\end{align}
%
where $\brc{\structureT{T}, \structureT{L}, \structureT{A}}$ are the \brc{transverse, longitudinal, asymmetric} fragmentation 
functions.
The scaling variable $z$ in the time-like case is usually defined as an inverse of the space-like \eqref{eq:bjorkenS}:
%
\begin{align}
    z = \frac{2 p \cdot q}{q^2}\,,
    \label{eq:bjorkenT}
\end{align}
%
so that the physical region remains the same: $0 < z < 1$. The time-like structure functions $\structureT{j}$ are
again factorized into coefficient functions $\coeffT{j, a}$ and the parton fragmentation functions $\ff{a}$:
%
\begin{align}
    \structureT{j} = \sbrk{\coeffT{j, a} \otimes \ff{a}}\,.
\end{align}
%
The time-like splitting functions $\splitting{ba}^+$ control the renormalization scale dependence of $\ff{a}$:
%
\begin{align}
    \frac{\dd \ff{a}\brk{x, \mu^2}}{\dd{\log\brk{\mu^2}}}
    = \sbrk{\splitting{ba}^+ \otimes \ff{b}}\brk{x}\,.
\end{align}
%
One can also introduce the time-like anomalous dimensions $\anomdim{ba}^+$ via:
%
\begin{align}
    \anomdim{ba}^+\brk{N, \alpha_s} = - \int\limits_0^1 \dd{\log\brk{x}} \mskip3mu x^N \splitting{ba}^+\brk{x, \alpha_s},
\end{align}
%
but, unlike the space-like \eqref{eq:anomdimS} case of DIS, these $\anomdim{ba}^-$ are no longer related to some
local Wilson operators. 

Based on the analysis in $\superN = 4$ SYM as well as the 3 loop QCD computations of
\cite{Moch:2004pa, Vogt:2004mw}, in \cite{Dokshitzer:2006nm, Basso:2006nk}
the following functional relation for the \brk{non-singlet} anomalous dimension was proposed:
%
\begin{align}
    \anomdim{\ns}^\sigma\brk{N} &= \univ{}\brk{N + \sigma \anomdim{\ns}^\sigma\brk{N} - \betafun{}\brk{a_s}/a_s}\,,
    \label{eq:univ}
\end{align}
%
where $\sigma \in \brc{-, +}$ stands for the \brc{space-like, time-like} kinematics. As a consequence of conformal
symmetry, \eqref{eq:univ} expresses both anomalous dimensions $\anomdim{\ns}^\pm$ in terms of one universal
function $\univ{}$. The large spin limit $N \to \infty$ of the universal function then takes the form
\cite{Dokshitzer:2005bf}:
%
\begin{align}
    \univ{}\brk{N}
	= A_n \brk{\log\brk{N} + \gamma_e} - B_n + C_n \brk{\log\brk{N} + \gamma_e} N^{-1}
	- \brk{D_n - \tfrac12 A_n} N^{-1},
    \label{eq:univLargeN}
\end{align}
%
which has only simple logarithmic terms $\brc{\log\brk{N}, \log\brk{N}N^{-1}}$ \cite{Korchemsky:1988si}.
%At finite $N$ they contain harmonic sums
%\remarks{ref} up to weight $2 L - 1$.
Using these constraints, in \cite{Moch:2017uml} 
the 4 loops non-singlet anomalous dimensions $\anomdim{\ns}^\sigma$ were reconstructed from fixed $N \in \brc{1, \ldots, 20}$ computations
in the planar limit $\Nc \to \infty$. Additionally, some non-planar corrections were considered 
and shown to contain $\zeta_5 \sbrk{S_1\brk{N}}^2 \xrightarrow[N \to \infty]{} \zeta_5 \sbrk{\log\brk{N}}^2$ term,
which violates the asymptotic behavior shown in \eqref{eq:univLargeN}. 

On the supersymmetric side, non-planar corrections to \brk{similar} universal anomalous dimensions were studied perturbatively at 4 loops in
\cite{Velizhanin:2008pc, Velizhanin:2009gv, Velizhanin:2010ey, Velizhanin:2014zla} up to spin $N = 8$
and there the same $\zeta_5 \sbrk{S_1\brk{N}}^2$ term was found. This discrepancy was then attributed to the so-called wrapping corrections,
which limits the integrability predictions in $\superN = 4$  SYM at the weak coupling \cite{Kotikov:2007cy, Bajnok:2008qj}.
The recent advance of computer software \cite{Ruijl:2017cxj, Ruijl:2017dtg} already made possible the 4 loop QCD calculations of
\cite{Moch:2017uml}, so it would be interesting to transfer this program to supersymmetric extensions of the Yang--Mills theory
and gather more information on the full non-planar contributions to the twist-2 universal anomalous dimensions.
%
%in terms of averaged anomalous dimensions $\baranomdim{n} = \frac12 \brk{\anomdim{n}^+ + \anomdim{n}^-}$
%\brk{note, that 1 loop space- and time-like coefficients are equal: $\baranomdim{0} = \anomdim{0}^+ = \anomdim{0}^-$}
%%
%\begin{align}
%    \univ{} = a_s \baranomdim{0} + a_s^2 \brk{\baranomdim{1} - \betafun{0} \brk{\baranomdim{0}}^\prime}
%    + a_s^3 \brk{
%        \baranomdim{2} - \tfrac16 \brk{\baranomdim{0}^3}^{\prime\prime}
%        - \betafun{1} \brk{\baranomdim{0}}^\prime
%        - \betafun{0} \brk{\baranomdim{1}}^\prime
%        + \tfrac12 \betafun{0}^2 \brk{\baranomdim{0}}^{\prime\prime}
%    }
%    + \order{a_s^4}
%\end{align}
%
% vim: fdm=syntax
