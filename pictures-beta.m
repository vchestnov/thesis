(* ::Package:: *)

(* ::Input:: *)
(*Module[{full = {{20, 20}, {70, 50}}, line, edgex = {20, 55}, edge, center, unit, arrow, label, len = 20, shift = 10},*)
(*line[x_] := InterpolatingPolynomial[full, x];*)
(*edge = {#, line @ #}& /@ edgex;*)
(*center =N @ Median @ edge;*)
(*unit = Transpose @ full . {1, -1} // # / Norm @ #& // N;*)
(*arrow = center + shift (RotationMatrix[-Pi / 2] . unit) // {# + unit len / 2, #- unit len / 2}&*)
(*// ToString // StringReplace[#, "}, " -> ")"]& // StringReplace[#, {"{" -> "(", "}" -> ")"}]&*)
(*// StringDelete[#, Alternatives[StartOfString ~~ "(", ")" ~~ EndOfString]]&;*)
(*label = center + 1.5 shift(RotationMatrix[-Pi / 2] . unit) *)
(*// ToString // StringReplace[#, "}, " -> ")"]& // StringReplace[#, {"{" -> "(", "}" -> ")"}]&*)
(*]*)


(* ::Input:: *)
(*Module[{full = {{120, 20}, {70, 50}}, line, edgex = {120, 85}, edge, center, unit, arrow, label, len = 20, shift = 10},*)
(*line[x_] := InterpolatingPolynomial[full, x];*)
(*edge = {#, line @ #}& /@ edgex;*)
(*center =N @ Median @ edge;*)
(*unit = Transpose @ full . {1, -1} // # / Norm @ #& // N;*)
(*arrow = center + shift (RotationMatrix[Pi / 2] . unit) // {# + unit len / 2, #- unit len / 2}&*)
(*// ToString // StringReplace[#, "}, " -> ")"]& // StringReplace[#, {"{" -> "(", "}" -> ")"}]&*)
(*// StringDelete[#, Alternatives[StartOfString ~~ "(", ")" ~~ EndOfString]]&;*)
(*label = center + 1.5 shift(RotationMatrix[Pi / 2] . unit) *)
(*// ToString // StringReplace[#, "}, " -> ")"]& // StringReplace[#, {"{" -> "(", "}" -> ")"}]&*)
(*]*)


(* ::Input:: *)
(**)


(* ::Input:: *)
(*Module[{full = {{45, 30}, {60, 10}}, line, edgex = {45, 60}, edge, center, unit, arrow, label, len = 10, shift = 5},*)
(*line[x_] := InterpolatingPolynomial[full, x];*)
(*edge = {#, line @ #}& /@ edgex;*)
(*center =N @ Median @ edge;*)
(*unit = Transpose @ full . {1, -1} // # / Norm @ #& // N;*)
(*arrow = center + shift (RotationMatrix[-Pi / 2] . unit) // {# + unit len / 2, #- unit len / 2}&*)
(*// ToString // StringReplace[#, "}, " -> ")"]& // StringReplace[#, {"{" -> "(", "}" -> ")"}]&*)
(*// StringDelete[#, Alternatives[StartOfString ~~ "(", ")" ~~ EndOfString]]&;*)
(*label = center + 1.5 shift(RotationMatrix[-Pi / 2] . unit) *)
(*// ToString // StringReplace[#, "}, " -> ")"]& // StringReplace[#, {"{" -> "(", "}" -> ")"}]&;*)
(*label*)
(*]*)


intersection[pt1_, v1_, pt2_, v2_] := Module[{t1, t2},
	pt1 + t1 v1 /. (Solve[pt1 + v1 t1 == pt2 + v2 t2, {t1, t2}] // First)
];

bisector[v1_, v2_] := Normalize[v1 + v2];


(* Split into thirds both vertical and horizontal space *)
Module[{
	file, proc, script,
	tostring, line, circle, vertex, text,
	sizex = 270, sizey = 200,
	marginx = 30, marginy = 30,
	tanE = .3, tanPr = -.5,
	eStart, eCusp, eEnd,
	eV1, eV2, qV1, qV2, pV,
	qStart, qCusp, qEnd,
	prStarts, prCusp1, prCusps, prEnds, prShift = 5,
	prV, prShiftV,
	mArrC, mArrL = 20, mArrShift = 10
},
	tostring[expr_] := expr // ToString // StringReplace[#, {"{" -> "(", "}" -> ")"}]&;
	line[pt1_, pt2_, command_:"Line", opt_:{}, arg_:{}] := {
		"\\", command,
		If[opt === {}, {}, {"[", opt, "]"}],
		tostring @* N /@ {pt1, pt2},
		arg
	} // Flatten // Map[ToString] // Apply[StringJoin];
	vertex[pt_, size_:2] := {
		"\\Vertex",
		tostring @* N @ pt,
		{"{", size, "}"}
	} // Flatten // Map[ToString] // Apply[StringJoin];
	circle[pt_, size_:9] := {
	"\\GCirc",
		tostring @* N @ pt,
		{"{", size, "}"},
		"{0.6}"
	} // Flatten // Map[ToString] // Apply[StringJoin];
	text[pt_, text_, opt_:{}] := {
		"\\Text",
		tostring @* N @ pt,
		If[opt === {}, {}, {"[", opt, "]"}],
		{"{", text, "}"}
	} // Flatten // Map[ToString] // Apply[StringJoin];
	
	eStart = {marginx, 2 / 3 sizey};
	qCusp = {1 / 2 sizex, 1 / 2 sizey};
	qStart = 1 / 3 {sizex, sizey};
	qEnd = {sizex - 2 marginx, 1 / 2 sizey};
	eV1 = {1, 0}; eV2 = Normalize[qCusp - qStart];
	qV1 = eV2; qV2 = eV1;
	pV = bisector @@ Normalize /@ {-eV1, eV2};
	
	prStarts = {marginx, 1 / 3 sizey} - # prShift {0, 1}& /@ Range[0, 2];
	prCusp1 = 1 / 3 {sizex, sizey};
	prV = Normalize @ {1, tanPr};
	prEnds = intersection[prCusp1, prV, {sizex, 5/4 marginy}, {-1, 0}] - # prShift (RotationMatrix[Pi / 2] . prV)&
		/@ Range[0, 2];
	prCusps = {prStarts, ConstantArray[{1, 0}, 3], prEnds, ConstantArray[-prV, 3]}
		// Transpose // Map[Apply[intersection]];
	
	
	eCusp = intersection[eStart, eV1, qCusp, pV];
	eEnd = intersection[eCusp, eV2, 2 / 3 {sizex, sizey}, {0, 1}];
	
	mArrC = (eCusp + qCusp) / 2 + mArrShift (RotationMatrix[-Pi / 2] . pV);
	
	script = Riffle[Flatten @ {
		line[eStart, eCusp, "Line", "arrow, arrowpos=0.5"],
		line[eCusp, eEnd, "Line", "arrow, arrowpos=0.5"],
		line[qCusp, eCusp, "Photon", {}, "{4}{4}"],
		line[qStart, qCusp, "Line", "arrow, arrowpos=0.5"],
		line[qCusp, qEnd, "Line", "arrow, arrowpos=0.5"],
		{prStarts, prCusps} // Transpose // Map[Apply[line[##, "Line"]&]],
		{prCusps, prEnds} // Transpose // Map[Apply[line[##, "Line"]&]],
		vertex[eCusp], vertex[qCusp],
		circle[prCusps[[2]]],
		text[eStart - {5, 0}, "electron", "rc"],
		text[prStarts[[2]] - {5, 0}, "proton", "rc"],
		text[qEnd + {5, 0}, "quark", "lc"],
		{"\\Red{",
			line[mArrC + mArrL pV / 2, mArrC - mArrL pV / 2, "Line", "arrow, arrowpos=1, arrowinset=0"],
			text[mArrC + mArrShift (RotationMatrix[-Pi / 2] . pV), "$q$", "rc"],
		"}"} // StringJoin
		
	}, "\n"] // Apply[StringJoin];
	
	script;
	proc = OpenWrite[FileNameJoin @ {NotebookDirectory[], "axo", "dis"}];
	WriteString[proc, script];
	Close[proc]
]


(* Split into thirds both vertical and horizontal space *)
Module[{
	file, proc, script,
	tostring, line, circle, vertex, text,
	sizex = 80, sizey = 70,
	marginx = 0, marginy = 10,
	tanE = .3, tanPr = -.5,
	eStart, eCusp, eEnd,
	eV1, eV2,
	pStart, pEnd, pV, pL = 30,
	cCenter,
	prStarts1, prCusps, prEnds1, prShift = 3,
	prStarts2, prEnds2,
	prLength,
	prV1, prShiftV1,
	prV2, prShiftV2,
	mArrC, mArrL = 20, mArrShift = 10
},
	tostring[expr_] := expr // ToString // StringReplace[#, {"{" -> "(", "}" -> ")"}]&;
	line[pt1_, pt2_, command_:"Line", opt_:{}, arg_:{}] := {
		"\\", command,
		If[opt === {}, {}, {"[", opt, "]"}],
		tostring @* N /@ {pt1, pt2},
		arg
	} // Flatten // Map[ToString] // Apply[StringJoin];
	vertex[pt_, size_:2] := {
		"\\Vertex",
		tostring @* N @ pt,
		{"{", size, "}"}
	} // Flatten // Map[ToString] // Apply[StringJoin];
	circle[pt_, size_:9] := {
	"\\GCirc",
		tostring @* N @ pt,
		{"{", size, "}"},
		"{0.6}"
	} // Flatten // Map[ToString] // Apply[StringJoin];
	text[pt_, text_, opt_:{}] := {
		"\\Text",
		tostring @* N @ pt,
		If[opt === {}, {}, {"[", opt, "]"}],
		{"{", text, "}"}
	} // Flatten // Map[ToString] // Apply[StringJoin];
	
	eCusp = 1 / 2 {sizex, sizey} - 1 / 2 {pL, 0};
	eStart = {marginx + 10, marginy};
	eEnd = {marginx + 10, sizey - marginy};
	pStart = eCusp;
	pV = {1, 0};
	pEnd = eCusp + pL pV - {0, 0};
	cCenter = pEnd + {9, 0};
	prV1 = Normalize[cCenter - {sizex - marginx, marginy}];
	(*prV1 = {1, 0};*)
	prV2 = Normalize[{sizex - marginx, sizey - marginy} - cCenter];
	prShiftV1 = RotationMatrix[-Pi / 2] . prV1;
	prShiftV2 = RotationMatrix[-Pi / 2] . prV2;
	
	prStarts1 = cCenter + # prShift prShiftV1& /@ Range[-1, 1];
	prStarts2 = cCenter + # prShift prShiftV2& /@ Range[-1, 1];
	prLength = Norm[cCenter - {sizex - marginx, sizey / 2}];
	prEnds1 = # - prV1 Norm[cCenter - {sizex - marginx, marginy}]& /@ prStarts1;
	prEnds2 = # + prV2 Norm[cCenter - {sizex - marginx, marginy}]& /@ prStarts2;
	
	mArrC = 1 / 2 (pStart + pEnd) - mArrShift {0, 1};
	
	script = Riffle[Flatten @ {
		line[eStart, eCusp, "Line", "arrow, arrowpos=0.5, arrowscale=.5"],
		line[eCusp, eEnd, "Line", "arrow, arrowpos=0.5, arrowscale=.5"],
		line[pStart, pEnd, "Photon", {}, "{3}{4}"],
		line[prStarts2[[2]], prEnds2[[2]], "Line", "arrow, arrowpos=0.6, arrowscale=.5"],
		{prStarts1, prEnds1} // Transpose // Map[Apply[line[##,"Line", "arrow, arrowpos=0.6, arrowscale=.5"]&]],
		vertex[pStart],
		circle[cCenter, 9],
		text[eStart - {2, 0}, "$e^-$", "rc"],
		text[eEnd - {2, 0}, "$e^+$", "rc"],
		text[prEnds1[[2]] + {2, 0}, "$X$", "lc"],
		text[prEnds2[[2]] + {2, 0}, "$h$", "lc"],
		{"\\Red{",
			line[mArrC - mArrL pV / 2, mArrC + mArrL pV / 2, "Line", "arrow, arrowpos=1, arrowinset=0"],
			text[mArrC + mArrShift (RotationMatrix[-Pi / 2] . pV), "$q$", "c"],
		"}"} // StringJoin
		
	}, "\n"] // Apply[StringJoin];
	
	script;
	proc = OpenWrite[FileNameJoin @ {NotebookDirectory[], "axo", "dis1"}];
	WriteString[proc, script];
	Close[proc]
]


(* Split into thirds both vertical and horizontal space *)
Module[{
	file, proc, script,
	tostring, line, circle, vertex, text,
	sizex = 80, sizey = 70,
	marginx = 0, marginy = 10,
	tanE = .3, tanPr = -.5,
	eStart, eCusp, eEnd,
	eV1, eV2,
	pStart, pEnd, pV, pL = 20,
	cCenter,
	prStarts1, prCusps, prEnds1, prShift = 3,
	prStarts2, prEnds2,
	prLength,
	prV1, prShiftV1,
	prV2, prShiftV2,
	mArrC, mArrL = 15, mArrShift = -10.,
	prArrC, prArrL = 15, prArrShift = 10,
	hEnd
},
	tostring[expr_] := expr // ToString // StringReplace[#, {"{" -> "(", "}" -> ")"}]&;
	line[pt1_, pt2_, command_:"Line", opt_:{}, arg_:{}] := {
		"\\", command,
		If[opt === {}, {}, {"[", opt, "]"}],
		tostring @* N /@ {pt1, pt2},
		arg
	} // Flatten // Map[ToString] // Apply[StringJoin];
	vertex[pt_, size_:2] := {
		"\\Vertex",
		tostring @* N @ pt,
		{"{", size, "}"}
	} // Flatten // Map[ToString] // Apply[StringJoin];
	circle[pt_, size_:9] := {
	"\\GCirc",
		tostring @* N @ pt,
		{"{", size, "}"},
		"{0.6}"
	} // Flatten // Map[ToString] // Apply[StringJoin];
	text[pt_, text_, opt_:{}] := {
		"\\Text",
		tostring @* N @ pt,
		If[opt === {}, {}, {"[", opt, "]"}],
		{"{", text, "}"}
	} // Flatten // Map[ToString] // Apply[StringJoin];
	
	eCusp = 1 / 2 {sizex, sizey} + 1 / 2 {0, pL};
	eStart = {marginx + 10, sizey - marginy};
	eEnd = {sizex - marginx - 10, sizey - marginy};
	pStart = eCusp;
	pV = {0, -1};
	pEnd = eCusp + pL pV;
	cCenter = pEnd - {0, 9};
	prV1 = Normalize[cCenter - {marginx + 10, marginy}];
	prV2 = Normalize[{sizex - marginx - 10, marginy} - cCenter];
	prShiftV1 = RotationMatrix[-Pi / 2] . prV1;
	prShiftV2 = RotationMatrix[-Pi / 2] . prV2;
	
	prStarts1 = cCenter + # prShift prShiftV1& /@ Range[-1, 1];
	prStarts2 = cCenter + # prShift prShiftV2& /@ Range[-1, 1];
	prLength = Norm[cCenter - {marginx + 10, marginy}];
	prEnds1 = # - prV1 prLength& /@ prStarts1;
	prEnds2 = # + prV2 prLength& /@ prStarts2;
	
	mArrC = 1 / 2 {sizex, sizey} + mArrShift {1, 0};
	hEnd = prEnds2[[1]] + {0, 15};
	prArrC = (prEnds1[[2]] + prStarts1[[2]]) / 2 + prArrShift prShiftV1;
	
	script = Riffle[Flatten @ {
		line[eStart, eCusp, "Line", "arrow, arrowpos=0.5, arrowscale=.5"],
		line[eCusp, eEnd, "Line", "arrow, arrowpos=0.5, arrowscale=.5"],
		line[pStart, pEnd, "Photon", {}, "{3}{3}"],
		(*{prStarts1, prEnds1} // Transpose // Map[Apply[line[##, "Line"]&]],*)
		line[prEnds1[[2]], prEnds1[[2]] + prV1 (Norm[prStarts1[[2]] - prEnds1[[2]]] - 9), "Line", "arrow, arrowpos=0.5, arrowscale=.5"],
		{prStarts2, prEnds2} // Transpose // Map[Apply[line[##, "Line", "arrow, arrowpos=0.6, arrowscale=.5"]&]],
		(*line[prStarts2[[1]], hEnd, "Line", "arrow, arrowpos=0.6, arrowscale=.5"],*)
		vertex[pStart],
		circle[cCenter, 9],
		text[eStart - {2, 0}, "$e^-$", "rc"],
		text[eEnd + {2, 0}, "$e^-$", "lc"],
		text[prEnds1[[2]] - {2, 0}, "$h$", "rc"],
		text[prEnds2[[2]] + {2, 0}, "$X$", "lc"],
		(*text[hEnd + {2, 0}, "$h$", "lc"],*)
		{"\\Red{",
			line[mArrC - mArrL pV / 2, mArrC + mArrL pV / 2, "Line", "arrow, arrowpos=1, arrowinset=0"],
			(*line[prArrC - prArrL prV1 / 2, prArrC + prArrL prV1 / 2, "Line", "arrow, arrowpos=1, arrowinset=0"],*)
			text[mArrC - mArrShift (RotationMatrix[-Pi / 2] . pV), "$q$", "c"],
		"}"} // StringJoin
		
	}, "\n"] // Apply[StringJoin];
	
	script;
	proc = OpenWrite[FileNameJoin @ {NotebookDirectory[], "axo", "dis2"}];
	WriteString[proc, script];
	Close[proc]
]


(* Split into thirds both vertical and horizontal space *)
Module[{
	file, proc, script,
	tostring, line, circle, vertex, text,
	sizex = 40, sizey = 80,
	marginx = 10, marginy = 10,
	xs, ys
},
	tostring[expr_] := expr // ToString // StringReplace[#, {"{" -> "(", "}" -> ")"}]&;
	line[pt1_, pt2_, command_:"Line", opt_:{}, arg_:{}] := {
		"\\", command,
		If[opt === {}, {}, {"[", opt, "]"}],
		tostring @* N /@ {pt1, pt2},
		arg
	} // Flatten // Map[ToString] // Apply[StringJoin];
	vertex[pt_, size_:2] := {
		"\\Vertex",
		tostring @* N @ pt,
		{"{", size, "}"}
	} // Flatten // Map[ToString] // Apply[StringJoin];
	circle[pt_, size_:9] := {
	"\\GCirc",
		tostring @* N @ pt,
		{"{", size, "}"},
		"{0.6}"
	} // Flatten // Map[ToString] // Apply[StringJoin];
	text[pt_, text_, opt_:{}] := {
		"\\Text",
		tostring @* N @ pt,
		If[opt === {}, {}, {"[", opt, "]"}],
		{"{", text, "}"}
	} // Flatten // Map[ToString] // Apply[StringJoin];
	
	xs = FoldList[#1 + #2&, {sizex / 2, marginy}, {
		{10, 10}, {0, 20}, {-10, 20},
		{-10, -10}, {0, -20}
	}];
	
	(*script = Riffle[Flatten @ {
		line[eStart, eCusp, "Line", "arrow, arrowpos=0.5, arrowscale=.5"],
		line[eCusp, eEnd, "Line", "arrow, arrowpos=0.5, arrowscale=.5"],
		line[pStart, pEnd, "Photon", {}, "{3}{3}"],
		(*{prStarts1, prEnds1} // Transpose // Map[Apply[line[##, "Line"]&]],*)
		line[prEnds1[[2]], prEnds1[[2]] + prV1 (Norm[prStarts1[[2]] - prEnds1[[2]]] - 9), "Line", "arrow, arrowpos=0.5, arrowscale=.5"],
		{prStarts2, prEnds2} // Transpose // Map[Apply[line[##, "Line", "arrow, arrowpos=0.6, arrowscale=.5"]&]],
		(*line[prStarts2[[1]], hEnd, "Line", "arrow, arrowpos=0.6, arrowscale=.5"],*)
		vertex[pStart],
		circle[cCenter, 9],
		text[eStart - {2, 0}, "$e^-$", "rc"],
		text[eEnd + {2, 0}, "$e^-$", "lc"],
		text[prEnds1[[2]] - {2, 0}, "$h$", "rc"],
		text[prEnds2[[2]] + {2, 0}, "$X$", "lc"],
		(*text[hEnd + {2, 0}, "$h$", "lc"],*)
		{"\\Red{",
			line[mArrC - mArrL pV / 2, mArrC + mArrL pV / 2, "Line", "arrow, arrowpos=1, arrowinset=0"],
			(*line[prArrC - prArrL prV1 / 2, prArrC + prArrL prV1 / 2, "Line", "arrow, arrowpos=1, arrowinset=0"],*)
			text[mArrC - mArrShift (RotationMatrix[-Pi / 2] . pV), "$q$", "c"],
		"}"} // StringJoin
		
	}, "\n"] // Apply[StringJoin];
	
	script*)(*;
	proc = OpenWrite[FileNameJoin @ {NotebookDirectory[], "axo", "dis2"}];
	WriteString[proc, script];
	Close[proc]*)
]


?FoldList
