\chapter{On analytic continuation of the heptagon remainder function}
\label{ch:spradlin}
%
In this chapter we want to compute the analytic continuation \brk{or the imaginary part}
of the scattering amplitude of $n = 7$ gluons in the planar $\superN = 4$ SYM theory.
More concretely, we study the divergence-free part of the amplitude ---
the remainder function $\RGS$ \eqref{eq:remainderDef} presented in \cite{Golden:2014xqf}.
We view $\RGS$ as a multivalued function of complexified
momenta $p_i$ of external particles and try to compute its analytic continuation
between the two important Riemann sheets. The starting point is the Euclidean sheet,
where the $\RGS$ was defined in \cite{Golden:2014xqf}. The endpoint is the
Mandelstam sheet, where an independent check of the $\RGS$ can be made in the
Multi Regge limit \cite{Bartels:2014jya, DelDuca:2016lad, DelDuca:2018hrv}.

Our main result is the explicit symbolic check of the \cite{Golden:2014xqf}
result in the collinear limit before and after the continuation between the
Euclidean and Mandelstam sheets.
Comparison with Multi Regge limit prediction of \cite{Bartels:2014jya, DelDuca:2016lad, DelDuca:2018hrv}
requires an additional transportation
of the result to a region of high energies in the Mandelstam sheet
and so far we have not been able to carry it out.
%
\section{Construction of the remainder function}
%
We start with a brief overview of \cite{Golden:2014xqf}, where the $n = 7$ gluon remainder
function was originally presented. The goal here is to introduce all the relevant notation and
try to motivate it to some extent. Since our approach to the analytic continuation of the $\RGS$
is very much brute-force, we do not spill all the mathematical details of the construction.
%%
%\subsection{Singularities of the remainder function}
%%
%Remainder function of a planar color-ordered
%amplitude \eqref{eq:remainderDef} as a multivalued function of complex momenta
%has singularities when a sum of consequtive momenta
%%
%\begin{align}
%    p_i + p_{i + 1} + \dots + p_{j − 1} = x_{i, j}
%    \label{eq:branchMomLocation}
%\end{align}
%%
%becomes null \brk{recall \eqref{eq:dualxDef}}. This is a consequence of locality and can be understood, for
%example, from the point of view of Feynman diagrams. Indeed let us consider the
%tree level case. If the external
%particles are ordered \brk{as they are in the color-ordered case}, then the
%only possible singularity can come from some Feynman propagator going on-shell.
%%
%\begin{equation}
%    \raisebox{-27.1pt}{
%        \begin{axopicture}(210,60)(-20,30)
%            \AxoGrid(-20,30)(10,10)(21,6){LightGray}{0.5}
%            %\Line(40,0)(40,120)
%            %\Line(120,0)(120,120)
%            %\Line(0,60)(200,60)
%            %\Line(80,0)(80,120)

%            \Line(40,60)(120,60)

%            \Line(0,75)(40,70)
%            \Line(0,60)(40,60)
%            \Line(0,45)(40,50)

%            \Line(170,85)(130,75)
%            \Line(170,70)(130,65)
%            \Line(170,50)(130,55)
%            \Line(170,35)(130,45)

%            \GCirc(40,60){20}{0.6}
%            \GCirc(130,60){20}{0.6}

%            %\Vertex(0,75){2}
%            %\Vertex(0,60){2}
%            %\Vertex(0,45){2}

%            \Vertex(24,72){2}
%            \Vertex(20,60){2}
%            \Vertex(24,48){2}

%            \Vertex(60,60){2}
%            \Vertex(110,60){2}

%            %\Vertex(170,85){2}
%            %\Vertex(170,70){2}
%            %\Vertex(170,50){2}
%            %\Vertex(170,35){2}

%            \Vertex(139.78, 77.445){2}
%            \Vertex(148.61, 67.326){2}
%            \Vertex(148.61, 52.674){2}
%            \Vertex(139.78, 42.555){2}

%            \Line[arrow,arrowpos=1](70,50)(100,50)
%            \Text(85,45)[ct]{$p_1 + p_2 + p_3$}

%            \Text(-3,75)[cr]{$p_1$}
%            \Text(-3,60)[cr]{$p_2$}
%            \Text(-3,45)[cr]{$p_3$}

%            \Text(173,85)[cl]{$p_4$}
%            \Text(173,70)[cl]{$p_5$}
%            \Text(173,50)[cl]{$p_6$}
%            \Text(173,35)[cl]{$p_7$}
%        \end{axopicture}
%    }
%    \xrightarrow{\text{Pole}}
%    \frac{1}{\brk{p_1 + p_2 + p_3}^2}
%\end{equation}
%%
%At the loop level the remainder function is a complicated transcendental
%function. Again locality allows its branch point singularities to occur only at
%$x_{i, j}^2 = 0$ \eqref{eq:branchMomLocation}.
%%
\subsection{Multivalued remainder function}
%
Before introducing the concrete expression for $\RGS$, let us first explain what we
should expect from it. The remainder function depends on complexified momenta $p_i$
of external particles or on dual momentum twistors $Z_i$, see \autoref{sec:kinematics}.
In case of the $\superN = 4$ SYM,
due to the dual conformal invariance, there are only $3 n - 15$ independent parameters
that describe the kinematics of the scattering process.

Being multivalued analytic function of the complexified kinematics, the remainder function
has many branch point
%\footnote{rather branch surfaces}
singularities or, equivalently,
many Riemann sheets or branches. For our purposes we only need two of them:
the Euclidean and the Mandelstam regions.
%
\subsubsection{Euclidean}
%
The Euclidean region is defined by two conditions: dual momenta $x_i$ \brk{see \autoref{sec:kinematics}} are kept real
and all non-consecutive $x_{ij}^2$ separations are space-like:
%
\begin{align}
    \brk{x_i - x_{i + 1}}^2 = 0\,, \qquad \brk{x_i - x_j}^2 < 0 \quad \text{otherwise}\,,
\end{align}
%
when evaluated in $\brk{\p\m\m\m}$ \brk{or $\brk{\p\p\m\m}$} signatures. The remainder
function is free of any singularities inside of this region, and only on the boundary
where some $x_{ij}^2 = 0$, branch points can appear. This analyticity property of the $\RGS$
inside of the Euclidean region greatly simplifies the construction of the remainder
function.
%
\subsubsection{Mandelstam}
%
The Mandelstam region is defined relative to the Euclidean region as follows \brk{see \cite{DelDuca:2018raq}}:
fix the energy signs of two incoming particles to be positive, fix all $x_{1j} < 0$ and rotate the momenta along
such a path, that:
%
\begin{align}
    \sgn\brk{x_{ij}^2} = \varrho_i \varrho_j, \quad 2 \le i < j \le 7\,,
\end{align}
%
where the signature $\varrho \equiv \brk{\varrho_i}_{3 \le i \le 7}$ collects
the signs of energies of the outgoing particles:
%
\begin{align}
    \varrho_i \equiv \sgn\brk{E_i}\,.
\end{align}
%
There are several different Mandelstam regions \cite{Bargheer:2015djt} labeled by $\varrho$ and we are
going to study only one of them:
%
\begin{align}
    \varrho = \brc{\p,\m,\m,\m,\p}
    \label{eq:region}
\end{align}
%
in order to check the concrete path
proposed in \cite{DelDuca:2018hrv} that should lead to this region.
%
\subsection{Remark on arguments and coordinates}
%
The expression for $\RGS$ is a 
linear combination of various polylogarithms and powers of $\pi$ with rational coefficients. 
Arguments that appear inside of polylogarithms in $\RGS$ have been deliberately crafted to simplify the analytic
structure of the total expression. In \cite{Golden:2014xqf} one representation of the heptagon remainder function was constructed,
such that only the so-called cluster $\clX$-coordinates appeared inside of the polylogarithms in the answer. These
coordinates are special cross ratios built from the cluster $\clA$-coordinates -- a set of coordinates on the
Grassmannian $\Gr{4, n}$, which represents the kinematical domain of the $n$-gluon scattering process \brk{see also \eqref{eq:kinDomain}}.
Cluster $\clA$-coordinates generalize the usual Pl\"ucker brackets 
$\vev{abcd} = \det\brk{Z_aZ_bZ_cZ_d}$ or slightly more complex objects:
%
\begin{align}
    \br{a\brk{bc}\brk{de}\brk{fg}}
    = \br{abde} \br{acfg} - \br{abfg} \br{acde}\,,
    \\
    \br{ab\brk{cde} \cap \brk{fgh}}
    = \br{acde} \br{bfgh} - \br{bcde} \br{afgh}\,.
\end{align}
%
In practice they evaluate to some homogeneous polynomials in components
of twistors $Z_i$. The entire set of $\clA$-coordinates is then produced by a simple algorithm defined within
the theory of cluster algebras \cite{Fomin:2002}.
A great review on cluster algebras and coordinates can be found in \cite{Golden:2013xva} and especially in \cite{Golden:2018gtk}.

One useful property of cluster $\clX$-coordinates is their positivity when evaluated on a certain subset of the
Euclidean region called the positive Grassmannian, where $\vev{abcd} > 0$ for ordered $a < b < c < d$. This
feature helps with writing manifestly well-defined \brk{at least on the positive Gra\ss mannian} expressions
in terms of classical polylogarithms \eqref{eq:clLitoG}. We see the effect of this positivity later when we parametrize
$\clX$-coordinates with WLOPE variables $\opevars_{i = 1, 2}$ \eqref{eq:crsBSV}.
%%
%Symbol \cite{CaronHuot:2011ky}, coproduct \cite{Golden:2013xva}, non-classical part of $\RGS$ \cite{Golden:2014xqa}
%and non-classical part of $R_n\Loops{2}$ \cite{Golden:2014pua}
%hexagon $R_6\Loops{2}$ \cite{Goncharov:2010jf}
%Cluster $\clA$-coordinates on $\Gr{4,7}$ \eqref{eq:kinDomain}
%
\section{OPE expansion in the Euclidean region}
%
The goal of this section is to derive the collinear expansion
of the 2-loop remainder function $\RGS$ of \cite{Golden:2014xqf}
and check that it reproduces the 1-gluon WLOPE contribution \eqref{eq:hep}.
The roadmap that we follow is outlined at the end of Section 5 of
\cite{Golden:2014xqf}, where the authors have used the collinear limit
in order to fix an overall constant in $\RGS$ numerically.
We use the machinery of \autoref{ch:mpl} to compute
the collinear expansion symbolically, since
this is going to be a part of the analytic continuation that we 
try to perform next in \autoref{sec:continuation}.
%
\subsection{Accounting of functions}
%
The remainder function $\RGS$ of \cite{Golden:2014xqf} is a $\Q$-linear
combination of weight 4 \brk{see \autoref{ch:mpl}}
functions and the expression contains the following 4 types of polylogarithms:
%
\begin{equation}
    \begin{array}{l | l}
        \text{fun} & \text{count} \\
        \hline
        G & 224 \\
        \Li_n^- & 576 \\
        \Li_n^+ & 28 \\
        \log & 217 \\
        \hline
        \text{total} & 933
    \end{array}
    \label{eq:funlist}
\end{equation}
%
where
%
\begin{itemize}
    \item $G$ stands for the non-classical weight 4 $G$-function which is
        packaged into $\Lfun$ in the original expression for $\RGS$
        \brk{compare to footnote 5 of \cite{Golden:2014xqf}}:
        \begin{align}
            \Lfun\brk{x, y}
            &=
            \tfrac12 G\brk{0, -\tfrac{1}{y}, 0, -\tfrac{1}{x}; 1} -
            \tfrac12 G\brk{0, -\tfrac{1}{x}, 0, -\tfrac{1}{y}; 1}
            \label{eq:L22toG}
            \\
            &=
            \tfrac12 \Li_{2, 2}\brk{-y, \tfrac{x}{y}} -
            \tfrac12 \Li_{2, 2}\brk{-x, \tfrac{y}{x}}\,.
            \nn
        \end{align}
        It is well-defined for $\brc{x, y} > 0$ and,
        therefore, on the positive Grassmannian as well.
    \item $\Li_n^\pm$ denote classical polylogarithms with explicit $\pm$ in
        front of their arguments. In particular, the $\Li_n^+$ can potentially
        be ill-defined even on the positive Grassmannian when its argument gets
        on the branch-cut. The full expression though is free of such spurious singularities.
\end{itemize}
%
So the functions appearing in \eqref{eq:funlist} in notation of
\eqref{eq:defG} can be combined into
%
\begin{align}
    \brc{G\brk{0, x, 0, y; 1}, G\brk{0^n, x; 1}, \log\brk{x}},
\end{align}
%
where the arguments are drawn from the set of $\clX$-coordinates on the Grassmannian $\Gr{4, 7}$.
%
\subsection{WLOPE parametrization of kinematics}
%
We substitute the BSV parametrization \brk{see \autoref{ch:amps} also \autoref{ch:opemrl}} of the momentum twistors $Z_i$ in $\RGS$. This explicitly
parametrizes the 49 cluster $\clA$-coordinates of $\Gr{4, 7}$ in terms of 6
WLOPE variables $\opevars_{i = 1, 2}$ \brk{see \autoref{sec:coll}}.
The motivation to do
this is that the path of analytic continuation of \autoref{sec:continuation} is
naturally given in terms of these particular variables.

The Euclidean region in the WLOPE variables corresponds to all $\opevars_{i = 1, 2} > 0$.
After substituting these variables in the arguments of
the functions in \eqref{eq:funlist} we see, that they become:
%
\begin{align}
    \Arg{\text{$\clA$-coordinates}}
    =
    \Arg{\brc{a_1, \ldots, a_{49}}}
    \xmapsto[\text{param}]{\text{WLOPE}}
    \Arg{\opevars}
    = -\frac{\mathcal{P}\brk{T_i, S_i, F_i}}{\mathcal{Q}\brk{T_i, S_i, F_i}}\,,
    \label{eq:argope}
\end{align}
%
where:
%
\begin{itemize}
    \item $\mathcal{P}$ and $\mathcal{Q}$ are polynomials in WLOPE variables
        with positive coefficients, which is a consequence of the positivity feature
        of the cluster $\clX$-coordinates mentioned above.
    \item $\Arg{\ldots}$ here and in the following represents an argument of some
        function \brk{not that of a complex number}.
    \item $\brk{-1}$ in front of the ratio is present in $\brc{G,
        \Li_n^-, \log}$ and makes these functions well-defined,
        but not in $\brc{\Li_n^+}$, which we always have to treat
        separately.
\end{itemize}
%
Thus the desired absence of branch cuts inside the Euclidean region is manifest
for almost every function in \eqref{eq:funlist}. The only exception are the $\Li_n^+$
classical polylogs, some of which indeed develop spurious branch singularities,
but the overall monodromy of $\RGS$ around any point in the interior of the
Euclidean region cancels.
%
\subsection{Collinear limit}
\label{ssec:coll}
%
Now it's time to specialize to the collinear limit $T_i \to 0$ for $i \in
\brc{1, 2}$. The behavior
of the remainder function here is captured by the WLOPE expansion \eqref{eq:hep},
so we want to reproduce these results up to
$\order{T_i}$ by expanding $\RGS$ of \cite{Golden:2014xqf}.
We build an asymptotic expansion of the remainder function
in products and powers of $\brc{T_i, \log\brk{T_i}}_{i = 1, 2}$
using methods of \autoref{sec:asymptotic} applied to the
$G$-functions that appear in $\RGS$. For each MPL we analyze the behavior of
its arguments as $T_i \to 0$ and generate an appropriate
identity that rewrites it in a form similar to \eqref{eq:logFactorGen}:
a divergent logarithm $\bigbrk{\log\brk{T_i}}^p$ times an analytic function at $T_i = 0$.
%
\subsubsection{Arguments in the collinear limit}
%
In order to select the expansion rule from \autoref{sec:asymptotic} 
we compute the leading term in the $T_i \to 0$ expansion of 
arguments of each function in \eqref{eq:funlist}. Recall that after
parametrization shown in \eqref{eq:argope} they become rational functions of the WLOPE variables
$\opevars_{i = 1, 2}$ and, up to overall factors, their behavior in the
collinear limit falls in one of the 3 possibilities:
%
\begin{align}
    \Arg{\opevars}
    \xrightarrow{T_i \to 0}
    \begin{cases}
        \pm T_i^{-n} & \text{pole,}
        \\
        \pm T_i^0 & \text{const,}
        \\
        \pm T_i^n & \text{zero.}
    \end{cases}
    \label{eq:argColl}
\end{align}
%
The triples \brc{$T_i$, $\pm$ sign, power $\pm n$} capture enough information
for choosing the expansion procedure of MPLs,
so we augment the list \eqref{eq:funlist} with them before further processing.
%
\subsubsection{Functions in the collinear limit}
%
Based on the collinear behavior of their arguments \eqref{eq:argColl},
$G$-functions from \eqref{eq:funlist} can be expanded in the collinear $T_i \to 0$
limit via appropriate identities from \autoref{sec:asymptotic}. They split
into 4 cases:
%
\begin{itemize}
    \item $\log\brk{x}$ with a positive argument: we factor
        out the explicit $\log\brk{T_i}$ terms if they are present, then expand
        the remaining logarithm with \eqref{eq:seriesLog}.
        %
    \item $G\brk{0^n, -x; 1}$ with a negative argument $-x < 0$ is a
        well-defined expression and we can directly apply \brk{generalizations
        of}
        \eqref{eq:seriesLiGen},
        \eqref{eq:dilogExampleLin} or \eqref{eq:dilogExampleNeg} depending on
        the behavior shown in \eqref{eq:argColl}.
        %
    \item $G\brk{0^n, x; 1}$ with a positive argument $0 < x < 1$ requires a regularization with $\pm i \eps$ as
        explained in \autoref{ssec:analytic}. One can interpret it as a choice
        of an analytic continuation path from negative argument $-x
        \xmapsto{\text{cont}} x_{\pm} \defas x \pm i \eps$
        as depicted in \autoref{fig:contourInv}.
        A consistent choice can be derived from $T_i \to T_i \pm i \eps$
        and the leading term of the $i \eps$-expansion of \eqref{eq:argope} in the
        Euclidean region $\opevars_{i = 1, 2} > 0$. After classification of \eqref{eq:argColl} we apply
        \eqref{eq:seriesLiGen}, \eqref{eq:dilogExampleLin} or
        \eqref{eq:dilogExample}.
        %
    \item $G\brk{0, -x, 0, -y; 1}$ with $\brc{x, y} > 0$ is again well-defined
        and we need to analyze the behavior of both arguments $\brc{x, y}$ to
        choose the correct identity \brk{see below}.
\end{itemize}
%
\subsubsection{Examples: dilogarithm and weight 4 function}
%
Now we give some concrete rules for building asymptotic expansions of the
classical polylogarithm $G\brk{0^n, -x; 1}$ and $G\brk{0, -x, 0, -y; 1}$ functions in terms of
$\brc{\log\brk{T_i}, T_i}$. Let us start with the dilogarithm $G\brk{0, -x; 1}$
\brk{see also (5.8)--(5.10) in \cite{Golden:2014xqf}}:
%
\begin{itemize}
    \item If $x$ goes to 0 as some power $T_i^n$:
        $x\brk{T_i} = \order{T_i^n}$,
        then we use \eqref{eq:dilogExampleNeg} to factor out the divergent
        $\log\brk{x}$:
        %
        \begin{align}
            G\brk{0, -x; 1} = \frac12 \bigbrk{\log\brk{x}}^2
            + \frac{\pi^2}{6} - G\brk{0, 1; x}\,.
            \label{eq:dilogZero}
        \end{align}
        %
    \item If $x$ goes to some finite value $x_0$: $x\brk{T_i} - x_0 =
        \order{T_i^n}$,
        then we use \eqref{eq:dilogExampleLin} to obtain the representation:
        %
        \begin{align}
            G\brk{0, -x; 1}
            &= G\brk{0, -x_0; 1}
            - G\brk{-x_0; 1} G\brk{-x_0; x - x_0}
            \nn
            \\
            &+ G\brk{-x_0, -x_0; x - x_0}
            - G\brk{-x_0, -1 - x_0; x - x_0}\,.
            \label{eq:dilogLin}
        \end{align}
        %
    \item If $x$ goes to $+\infty$: $\bigbrk{x\brk{T_i}}^{-1} = \order{T_i^n}$,
        then we rescale
        \eqref{eq:rescaling}:
        %
        \begin{align}
            G\brk{0^n, x; 1} = G\brk{0^n, 1; x^{-1}}\,.
            \label{eq:dilogInf}
        \end{align}
\end{itemize}
%
In all of the three cases above the next step is to use \eqref{eq:GtoLi} and
\eqref{eq:seriesLiGen} and produce the series expansion of the remaining
$G$-functions. Note, that we only get divergent logarithms in
\eqref{eq:dilogZero} when the argument $x$ hits the basepoint of integration 0.
Similar manipulations produce asymptotic expansions of general
classical polylogarithms $G\brk{0^n, x; 1}$, but we do not discuss this
further.

Next is a bit more involved case of $G\brk{0, -x, 0, -y; 1}$
\brk{see also (5.11)--(5.13) in \cite{Golden:2014xqf}}:
%
\begin{itemize}
    \item If $x$ goes to 0 faster than $y$: $\frac{x\brk{T_i}}{y\brk{T_i}} =
        \order{T_i^n}$ then, using methods of \autoref{sec:asymptotic},
        we generate an identity:
        %
        \begin{align}
            G\brk{0, -x, 0, -y; 1} = G\brk{0^3, -y; 1} + \order{x}\,.
        \end{align}
        %
        Or if $y$ goes to 0 faster than $x$:
        %
        \begin{align}
            G\brk{0, -x, 0, -y; 1}
            = \frac12 \bigbrk{\log\brk{y}}^2 G\brk{0, -x; 1}
            + 2 \log\brk{y} G\brk{0^2, -x; 1}
            \nn\\
            + 3 G\brk{0^3, -x; 1}
            + \frac{\pi^2}{6} G\brk{0, -x; 1}
            + \order{y}\,.
        \end{align}
        %
        In both cases, if the second argument also vanishes,
        we can use \eqref{eq:dilogZero} to further rewrite the
        classical polylogarithms on the RHS.
    \item If $x$ goes to $+\infty$ faster than $y$:
        $\frac{y\brk{T_i}}{x\brk{T_i}} = \order{T_i^n}$ then we build the
        following rule:
        %
        \begin{align}
            G\brk{0, -x, 0, -y; 1}
            &=
            G\brk{0, 1; -\frac1x} G\brk{0, -y; 1} + G\brk{0, -\frac1y; -\frac1x} G\brk{0, -y; 1}
            \nn
            \\
            &- 2 G\brk{-y; 1} G\brk{0^2, 1; -\frac1x} + 2 G\brk{-y; 1} G\brk{0^2, -\frac1y; -\frac1x}
            \nn
            \\
            &+ 3 G\brk{0^3, 1; -\frac1x} - 2 G\brk{0^2, -\frac1y, 1; -\frac1x} -
            G\brk{0, -\frac1y, 0, 1; -\frac1x}\,.
        \end{align}
        %
        Or if $y$ goes to $+\infty$ faster than $x$:
        %
        \begin{align}
            G\brk{0, -x, 0, -y; 1}
            = G\brk{0, -\frac1x; -\frac1y} G\brk{0, -x; 1} + G\brk{0^3, 1; -\frac1y} -
            G\brk{0, -\frac1x, 0, 1; -\frac1y}\,.
        \end{align}
        %
        In both cases the RHS is actually polynomially small and no divergent
        logarithms are generated.
    \item If both $x$ and $y$ go to some finite values: $x - x_0 =
        \order{T_i^n}$ and $y - y_0 = \order{T_i^m}$ then the expansion is
        again free of logarithms:
        %
        \begin{align}
            G\brk{0, -x, 0, -y; 1} = G\brk{0, -x_0, 0, -y_0; 1} + \order{\brk{x
            - x_0}, \brk{y - y_0}}\,,
        \end{align}
        %
        and the produced identity is even less intelligible than the ones
        above, so we omit it altogether.
\end{itemize}
%
This covers all the patterns that appear in \eqref{eq:funlist}, so that we can
generate the asymptotic expansion of $\RGS$ in the collinear limit up to 
$\order{T_i}$. 
One could optimize the expansion procedure to generate a couple of the subleading orders of $T_i^n$,
if needed.
%
\subsection{Remainder function in the collinear limit}
%
Here we can state the checks of the 2 loop remainder function $\RGS$
\cite{Golden:2014xqf} that we performed. Using the
technology from the previous subsection, we generate the asymptotic expansion of
the $\RGS$ in terms of $\brc{T_i, \log\brk{T_i}}_{i = 1, 2}$ of the following form:
%
\begin{align}
    \RGS
    &= \sum_{i = 1, 2} \cos\brk{\phi_i} T_i \brk{
        \tilde{f}_2^{\brk{0}}
        + \log\brk{T_i} \tilde{f}_2^{\brk{1}}
    }
    \\
    &+ \cos\brk{\phi_1 + \phi_2} T_1 T_2 \brk{
        \tilde{h}_2^{\brk{0, 0}}
        + \log\brk{T_1} \tilde{h}_2^{\brk{1, 0}}
        + \log\brk{T_2} \tilde{h}_2^{\brk{0, 1}}
    }
    \\
    &+ \cos\brk{\phi_1 - \phi_2} T_1 T_2 \tilde{\bar{h}}_2^{\brk{0, 0}}
    + \wl^{\supBDS}_7
    + \order{T_1^2, T_2^2}\,,
\end{align}
%
and check that all $\brc{\tilde{f}_2^{\brk{i}}, \tilde{h}_2^{\brk{i, j}}}$
reproduce the corresponding $\brc{f_2^{\brk{i}}, h_2^{\brk{i, j}}}$
in the WLOPE \brk{see \cite{Basso:2013aha}, also \autoref{ch:opemrl}}. Our analysis here is completely
symbolic, although originally in
\cite{Golden:2014xqf} the same check was performed numerically, so we do not
contribute something exceptionally new here. Our main motivation was to test the code
for asymptotic expansions that is going to be used in the following section.
%
\newpage
%
\section{Analytic continuation to Mandelstam region}
\label{sec:continuation}
%
\begin{wrapfigure}[13]{r}[0pt]{0pt}
    \centering
    \includegraphicsbox{continuation2}
    \caption{Path of analytic continuation $\genM$ in the $U_{2, 6}$-plane goes from the Euclidean
        region, into the collinear limit \brk{gray box}, rotates around
        the branch point $U_{2, 6} = 0$ and goes out to the Mandelstam
        region.
    }
    \label{fig:genMDefT2}
\end{wrapfigure}

Now we describe our attempt at the analytic continuation of the remainder
function away from the Euclidean region, where it was originally built
\cite{Golden:2014xqf}. Our goal is the Mandelstam region \eqref{eq:region},
where an alternative approach of \cite{Bartels:2011ge}
\brk{see also a more recent \cite{DelDuca:2016lad, DelDuca:2018hrv}}
gives an asymptotic expansion of $\RGS$ in the multi-Regge limit
\brk{MRL}.
%
\subsection{Continuation in terms of cross-ratios}
%
We are interested in two branches of the remainder function $\RGS$: the
Euclidean and one of the Mandelstam regions \eqref{eq:region}.
From the point of view of kinematics, these two domains are connected via
a rotation of the $U_{2, 6}$ cross-ratio around 0:
%
\begin{align}
    %U_{2, 6} \xrightarrow[\brk{\m\m\m}]{} e^{2 \pi i} U_{2, 6}
    \genM : U_{2, 6} \mapsto e^{2 \pi i} U_{2, 6}\,,
    \label{eq:genMDef}
\end{align}
%
which we label by $\genM$ and shown in \autoref{fig:genMDefT2}.

We also would like to narrow the kinematical domain on each branch in question
to a certain high-energy corner of the Multi-Regge limit.
In terms of cross-ratios $U_{i, j}$, the strict ordering of energies
\eqref{eq:mrlEnergies} in the MRL results into the following boundary values:
%
\begin{align}
    \brc{U_{2, 5}, U_{3, 6}, U_{2, 6}} &\xrightarrow[\mathrm{MRL}]{} 1\,,
    \nn
    \\
    \brc{U_{1, 4}, U_{1, 5}, U_{3, 7}, U_{4, 7}} &\xrightarrow[\mathrm{MRL}]{} 0\,,
    \label{eq:crsMRL}
\end{align}
%
with $U_{2, 6}$ being the biggest of all. This kinematical limit has different
consequences on different branches of $\RGS$. In the Euclidean domain
the remainder function of \cite{Golden:2014xqf} vanishes:
%
\begin{align}
    \RGS \xrightarrow[\mathrm{MRL}]{} 0\,.
\end{align}
%
But, once  continued in the Mandelstam region, $\RGS$ acquires
a non-zero leading logarithmic term in the asymptotic expansion of MRL:
%
\begin{align}
    \genM\brk{\RGS} \xrightarrow[\mathrm{MRL}]{}
    f\brk{\eps_1, w_1} + f\brk{\eps_2, w_2}
    + g\brk{w_1, w_2}
\end{align}
%
\brk{see \autoref{ch:opemrl}}, where the MRL is parametrized by vanishing $\eps_i \to 0$.
%
\subsection{Continuation in WLOPE variables}
\label{ssec:contWLOPE}
%
In our approach we chose a different parametrization of the kinematical
domain in terms of WLOPE variables $\opevars$ \eqref{eq:crsBSV} following
the discussion in Section 4.1 of \cite{DelDuca:2018hrv}. In these variables,
the rotation \eqref{eq:genMDef} is modeled by reducing $T_1 \ll 1$ in \eqref{eq:crsBSV}
to small values \brk{which enter the collinear limit}, then rotating
%
\begin{align}
    %T_1^2 \xrightarrow[\brk{\m\m\m}]{} e^{2 \pi i} T_1^2
    \genM : T_1 \mapsto e^{\pi i} T_1
    \label{eq:genMDefT}
\end{align}
%
around 0 and scaling $T_1$ back as shown in \autoref{fig:genMDefT}, since
$U_{2, 6} \propto T_1^2$ in that limit \eqref{eq:crsBSVcoll}.
The proper MRL is obtained then by increasing both $\abs{T_{1, 2}} \to \infty$,
but keeping them separated $\abs{T_1} \gg \abs{T_2}$ and keeping $\brc{S_i, F_i}_{i = 1, 2}$ fixed.
Note, that during that scaling to $\infty$, cross ratios do not leave the
%
\begin{align}
    0 < U_{i, j} < 1
    \label{eq:crsDomain}
\end{align}
%
bounds \brk{if $\brc{S_i, F_i}_{i = 1, 2} > 0$}, so no other
branch cut of the $\RGS$ is crossed.

\begin{wrapfigure}[13]{r}[0pt]{0pt}
    \centering
    \includegraphicsbox{continuation}
    \caption{Path of analytic continuation $\genM$ in the $T_1$-plane goes from Euclidean
        region, goes into the collinear limit \brk{gray box}, rotates around
        the branch point $T_1 = 0$ and goes out to Mandelstam
        region at $T_1 < 0$, where spurious branch points of
        individual MPLs appear.
    }
    \label{fig:genMDefT}
\end{wrapfigure}
%
So the analytic continuation in question consists of 3 steps: transporting
$\RGS$ from general kinematics in the Euclidean region into the collinear limit
$T_i \ll 1$, then rotation \eqref{eq:genMDefT}, pulling the answer out of
the collinear limit and moving it even further into the MRL $T_i \gg 1$. The
first stage is easy to do, since the $\RGS$ was constructed to be \brk{almost} free of
spurious branch points inside of the Euclidean region. That means that we can
freely scale $T_i \ll 1$ without any branch issues. The following rotation
\eqref{eq:genMDefT} is then also clear. The last scaling thought is more
difficult, since the discontinuity of $\RGS$ has many spurious branch points in
the Mandelstam region. So far we did not succeed at this step.
%
\subsection{Rotation around branch point}
%
The arguments of functions \eqref{eq:funlist} appearing in $\RGS$ are rational
expressions in the WLOPE variables
$\opevars$ \eqref{eq:argope}, so for them the continuation \eqref{eq:genMDefT} 
results in a change of sign:
%
\begin{align}
    \genM : \Arg{\opevars_{i = 1, 2}} \mapsto \Arg{\brc{-T_1, T_2, S_i, F_i}_{i = 1, 2}}
    \label{eq:genMArg}
\end{align}
%
But since MPLs might develop branch cuts at $T_1 = 0$, we need more information
about the way this transition to negative values goes. Namely, for each argument we need its
winding number around $T_1 = 0$, which is captured by the leading term
in the $T_1 \to 0$ expansion \eqref{eq:argColl}. It turns out, that only
leading powers $T_1^n$ with $\abs{n} \le 2$ appear in these expansions:
%
\begin{itemize}
    \item $n = 2$: the argument rotates once around 0, so the corresponding MPL
        picks up a discontinuity term.
    \item $n = -2$: the argument rotates once around $\infty$, no additional terms arise.
    \item $n = \brc{\pm 1}$: the argument only changes its sign, i.e. it does a
        half-winding around 0 or $\infty$, no additional terms arise.
    \item $n = 0$: the argument moves around some finite point, no additional terms arise.
        \brk{unless this is the other endpoint}
\end{itemize}
%
In this classification, arguments with $n \ne 2$ do not produce any
discontinuities of their MPL as they follow \eqref{eq:genMDefT}. Only if the
argument $x$ behaves like $T_1^2$ it winds around 0 and generates a discontinuity
$\Disc{0}{x}$. Schematically, the action of \eqref{eq:genMArg} on the arguments of
some MPL $G\brk{\vv{a}; z}$ induces:
%
\begin{align}
    \genM : G\brk{\vv{a}; z} = G\brk{\vv{a}; z}\bigg\rvert_{T_1 \to -T_1} + \Disc{0}{x}G\brk{\vv{a}; z}
    \label{eq:genMG}
\end{align}
%
when there is an argument $x \in \vv{a}$ that rotates around 0 during
the continuation \eqref{eq:genMDefT}. For the functions in \eqref{eq:funlist} we
then need to distinguish only three cases:
%
\begin{itemize}
%
    \item Logarithm:
        \begin{align}
            \Disc{0}{x} G\brk{0^n; x} =
            \frac{1}{n!} \Disc{0}{x} \bigbrk{G\brk{0; z}}^n =
            \frac{1}{n!} \bigbrk{2 \pi i + G\brk{0; z}}^n\,.
            \label{eq:discLog}
        \end{align}
%
    \item Classical polylogarithm: 
%
\begin{align}
    \Disc{0}{x} G\brk{0^n, x; 1}
    = \frac{2 \pi i}{n!} \bigbrk{G\brk{0; x}}^n\,.
    \label{eq:discLi}
\end{align}
%
    \item Weight 4 function:
%
\begin{align}
    \Disc{0}{x} G\brk{0, x, 0, y; 1}
    &=
    - 2 \pi i G\brk{0; x} G\brk{0, y; x}\,,
    \nn
    \\
    \Disc{0}{y} G\brk{0, x, 0, y; 1}
    &= 2 \pi i \bigbrk{
        - G\brk{0; y} \bigbrk{
            G\brk{0, x; y} + G\brk{0, x; 1}
        }
        + 2 G\brk{0^2, x; y}
        - 2 G\brk{0^2, x; 1}
    }\,.
    \label{eq:discLi22}
\end{align}
%
\end{itemize}
%
Let us give more details on the last point here. The weight 4 function $G\brk{0, x,
0, y; 1}$ has 2 "degrees of freedom" and, therefore, very delicately depends on
the motion of its arguments $\brc{x, y}$.
%\remarks{example \& pic?}
Luckily, functions in \eqref{eq:funlist} do not experience such complicated contour
deformations during \eqref{eq:genMDefT} and at most only one of their arguments
rotates around $T_1 = 0$, in which case we can use the appropriate rule from
\eqref{eq:discLi22}.

In summary, \eqref{eq:genMG} together with \eqref{eq:discLog}, \eqref{eq:discLi}, and
\eqref{eq:discLi22} outline the analytic continuation procedure along the path
\eqref{eq:genMDefT} and bring the multivalued remainder function $\RGS$ to the Mandelstam
branch. The final step is then to take the $\abs{T_i} \to
\infty$, which corresponds to the Multi-Regge limit and constitutes the connection
between answers of \cite{Golden:2014xqf} and \cite{Bartels:2011ge,
DelDuca:2018hrv}.
%
\subsection{Transition to the MRL}
%
Finally, let us comment on the last barrier that separates us from the
Multi-Regge limit of the remainder function $\RGS$ and it is the problem of
transporting the result of the above rotation \eqref{eq:genMDefT} around $T_1 = 0$ from $\abs{T_1}
\ll 1$ to $\abs{T_1} \gg 1$. This task seems to be a lot more involved than a
similar transition from the general Euclidean kinematics into the collinear
limit that we performed in \autoref{ssec:coll} due to the presence of spurious
branch points as shown in \autoref{fig:genMDefT}. As it was mentioned in
\autoref{ssec:contWLOPE}, during that MRL transition $\abs{T_i} \to \infty$,
cross ratios stay in the range \eqref{eq:crsDomain} and hence $\RGS$ does not acquire
any further discontinuity as a whole regardless of the particular path of
continuation in the MRL. But individual MPLs very much do change
as we move $T_1 \to -\infty + i \eps$, so we are forced to track of how every
argument \eqref{eq:argope} winds around branch points. To make this more
tractable, we would like to numerically parametrize the path of continuation
\eqref{eq:genMDefT} by fixing all free variables \brk{e.g. $\brc{F_i, S_i}_{i =
1, 2}$}, so that the arguments \eqref{eq:argope} become rational functions of
only $T_1$. Then we will be able to explicitly track how these arguments wind around
branch points of all MPLs and hence compute the analytic continuation of $\RGS$
into the MRL along the chosen numeric path. 
%
\section{Summary}
%
In this chapter we showed one application of the theory of multiple
polylogarithms to the problem of analytic continuation of the 7 gluon MHV
remainder function $\RGS$ at 2 loops. That multivalued function is explicitly given on one
of its "unphysical" branches \brk{the Euclidean region} in \cite{Golden:2014xqf},
so our goal was to continue $\RGS$ into a physical branch \brk{the Mandelstam
region} following the path of continuation outlined in
in \cite{DelDuca:2018hrv}. In the former reference, the high-energy
\brk{Multi-Regge} limit of $\RGS$ was bootstrapped on the Mandelstam branch and
our intention was to explicitly test their result.

As a part of a bigger program, in our work we checked
that the asymptotic expansion of the remainder function of \cite{Golden:2014xqf}
in the collinear limit reproduces independent results of \cite{Basso:2013aha}.
This computation was purely symbolic \brk{while the original check was numeric},
which provides a non-trivial check of our setup.
We were also able to analytically continue the remainder function to the
physical branch and checked that it again agrees with \cite{Basso:2013aha},
which has a simple continuation in that region.

So far, we were not able to do the last step of taking the high energy limit
of the remainder function on the Mandelstam sheet. This requires a much finer
analysis and we leave it for future work.
%
% vim: fdm=syntax
