\section{Basics}
\label{sec:iintintro}
%
The problem of integrating rational functions is somewhat old.
It dates back to works of Kummer \cite{Kummer1840}, who studied the first
examples of iterated integrals, then generalized by Poincar\'e \cite{poincare1884}
and Lappo-Danilevsky \cite{LappoCorps}, who introduced the notion of
a hyperlogarithm and then developed their theory in a three-volume series
%\footnote{%
%It seems that these were published posthomously: Lappo-Danilesky died in
%1931, shortly after moving from St.~Petersburg to Hessen}
starting with \cite{Lappo1}.
Further developments of \cite{Chen:1977oja} resulted, in particular, in a condition for the homotopy
invariance of the iterated integral as a function of the integration contour.
Then the algebraic structure of multiple polylogarithms was studied in \cite{Goncharov:2001iea}
and in \cite{Brown:2009qja}. This very quickly led to tremendous
progress in the area of scattering amplitudes in $\superN = 4$ supersymmetric
Yang--Mills theory: starting from the original application of the Hopf algebra
technology to 6 gluon scattering at 2 loops in \cite{Goncharov:2010jf, Golden:2013xva},
and then at 3 loops in \cite{Dixon:2011pw, Dixon:2013eka} and at 4 loops in
\cite{Dixon:2014voa}. Obtaining results of such complexity using the standard diagrammatic
methods is not feasible, so it should give us enough motivation for a thorough
study of the language of these function before actually doing physics
with them.
%
\subsection{Definitions}
%
\begin{wrapfigure}[7]{r}[0pt]{0pt}
    \centering
    \includegraphicsbox{defG}
    \caption{Iterated integral representation of a MPL.}
    \label{fig:defG}
\end{wrapfigure}
%
A multiple polylogarithm \brk{MPL} or a $G$-function or a hyperlogarithm is defined as:
%
\begin{equation}
    G\brk{a_1, \dots, a_k; z} =
    \int\limits_{\gamma_z} \! \frac{\dd t_1}{t_1 - a_1} G\brk{a_2, \dots, a_k; t_1}\,,
    \label{eq:defG}
\end{equation}
%
where:
%
\begin{itemize}
    \item All arguments $a_i \in \C$.
    \item $\gamma_z : \sbrk{0, 1} \to \C$ is the integration path from $0$ to endpoint $z \in \C$.
        Usually it is assumed to go straight along the real line.
    \item $G\brk{;z} = 1$ terminates the recursive definition
\end{itemize}
%
\subsubsection{Some basic properties}
%
Let us give some examples of MPLs.
A function with identical arguments is just a logarithm:
%
\begin{align}
    G\brk{a, \ldots, a; z}
    = \frac{1}{k!} \Bigbrk{\log\bigbrk{1 - \frac{z}{a}}}^k,
    \label{eq:logDefGen}
\end{align}
%
and in the case of $a = 0$, as a part of the shuffle regularization, we define it to be:
%
\begin{align}
    G\brk{0^k; z}
    \equiv
    G\brk{
        \underbrace{
            0, \dots, 0
        }_{
            k
        };
        z
    }
    = \frac{1}{k!} \bigbrk{\log{z}}^k.
    \label{eq:logDef}
\end{align}
%
Another property that follows from \eqref{eq:defG} is the rescaling invariance
of arguments for $a_k \ne 0$:
%
\begin{align}
    G\brk{a_1, \dots, a_k; z}
    = G\brk{x a_1, \dots, x a_k; x z}.
    \label{eq:rescaling}
\end{align}
%
We do not make any distinction between terms "multiple
polylogarithm" and "hyperlogarithm" in this text, even though they label different objects
in mathematical literature \cite{Panzer:2014caa}.
%
\subsection{Analytic structure}
\label{ssec:analytic}
%
Algorithmic processing of MPLs that we discuss later abstracts from the
concrete integral \eqref{eq:defG} and deals with just a list of its
arguments. This provides an efficient data structure, but also requires us to
check constantly, that the expressions we produce would make
sense once we re-introduce the integral notation. So in this subsection we
summarize results about singularity structure of $G$-functions.
%
\subsubsection{Branch cuts}
%
MPLs have branch cuts whenever one of its arguments crosses the integration contour.
Let us showcase this in pictures.
Consider some $G$-function, fix the path of integration in \eqref{eq:defG}
to be a straight line
from 0 to some $z \in \R_{>0}$ and let $a \in \C$ be one of its arguments,
that initially lies outside of the path. A possible analytic continuation with
respect to $a$ that reveals the branch cut structure is:
%
\begin{align}
    \includegraphicsbox{branchCut1}
    %
    %\xrightarrow{\text{Step 1}}
    \xmapsto{\text{Step 1}}
    %\xrightarrow{\mathmakebox[2em]{\text{Step 1}}}
    %
    \includegraphicsbox{branchCut2}
    %
    %\xrightarrow{\mathmakebox[2em]{\text{Step 2}}}
    %\xrightarrow{\text{Step 2}}
    \xmapsto{\text{Step 2}}
    %
    \includegraphicsbox{branchCut3}
    \label{eq:analytContEx}
\end{align}
%
where we start with a well-defined function, then
after Step 1 we need to shift $a \to a + i \eps$ in order to define
the branch and after Step 2 a loop around $a$ appears, which
represents the discontinuity.

So in general, whenever an argument $a$ crosses the contour of integration, a
small shift is required to make sense of the $G$-function:
%
\begin{align}
    G\brk{\ldots, a, \ldots; z} \to G\brk{\ldots, a \pm i \eps, \ldots; z}\,.
\end{align}
%
We deduce the sign here from the previous movement of the argument $a$ in the complex plane $\C$.
In \autoref{sec:iintloops} we study analytic continuation \eqref{eq:analytContEx} in more detail.
%
\subsubsection{Endpoint singularities and shuffle regularization}
%
Another type of divergence appears when the endpoint of integration $z$
is on the left, or 0 \brk{the basepoint of integration} is on the right in the list of arguments:
%
\begin{align}
    G\brk{z, \dots; z} \quad \text{or} \quad
    G\brk{\dots, 0; z}\,.
    \label{eq:endpointSing}
\end{align}
%
These $G$-functions can be consistently made sense of using the shuffle
regularization \cite{Brown:2009qja, Panzer:2015ida}, a combinatorial operation
on lists which factors out the divergent substrings of arguments:
%
\begin{align}
    G\brk{a_1, \ldots, a_n, 0^k; z}
    = \sum\limits_{i = 0}^k G\brk{0^i; z} f^{\brk{i}}_{\vv{a}, 0}\brk{z}\,,
    \label{eq:logFactorZero}
\end{align}
%
where:
%
\begin{itemize}
    \item $a_n \neq 0$ so that all zeros are combined into $0^k$.
    \item $f^{\brk{i}}_{\vv{a}, 0}\brk{z}$ are analytic at $z \to 0$.
    \item We can further use \eqref{eq:logDef} on the RHS to rewrite
        $G\brk{0^i; z}$ in terms of powers of $\log\brk{z}$.
    \item $G\brk{z, \ldots, a, \ldots; z}$ admits a similar decomposition, although in
        that case we need to introduce a formally divergent \brk{see
        \eqref{eq:logDefGen}} expression
        $G\brk{z, \ldots, z; z}$ instead of $G\brk{0^i; z}$.
\end{itemize}
%
We postpone further discussion until \autoref{ssec:shuffleReg} because we,
following \cite{Panzer:2015ida}, need to introduce proper algebraic language
for dealing with MPLs in \autoref{sec:iintloops} first.
%
\subsubsection{Series representation}
%
The efficient numerical evaluation of MPLs is based on their Taylor expansion
\cite{Vollinga:2004sn} rather than very computationally demanding multiple integration from
the definition \eqref{eq:defG}.
A $G$-function \eqref{eq:defG} has a convergent series representation when:
%
\begin{align}
    \abs{z} \le \abs{a_j} \quad \text{
        for all $j$ such that $a_j \ne 0$
    },
    \label{eq:expanCond}
\end{align}
%
and if $m_1 = 1$ then the ratio $\tfrac{y}{z_1}$ should not be equal to $1$.
To describe the expansion it is useful to introduce the $\Li$-notation
\brk{multiple polylogarithm in mathematical literature
\cite{Goncharov:2001iea}}:
%
\begin{align}
    \Li_{m_1, \dots, m_k}\brk{x_1, \dots, x_k}
    = \sum_{i_1 > \dots > i_k > 0}
    \frac{x_1^{i_1}}{i_1^{m_1}} \dots
    \frac{x_k^{i_k}}{i_k^{m_k}}\,,
    \label{eq:seriesLiGen}
\end{align}
%
which is convergent when:
%
\begin{align}
    \abs{x_1 \dots x_j} < 1 \quad \text{
        for all $j \le k$ and $\brk{m_1, x_1} \ne \brk{1, 1}$
    }.
\end{align}
%
Conversion between the $G$ and $\Li$ notation is given by:
%
\begin{align}
    \Li_{m_1, \dots, m_k}\brk{x_1, \dots, x_k}
    &= \brk{-1}^k G\brk{
        0^{m_1 - 1}, \tfrac{1}{x_1},
        0^{m_2 - 1}, \tfrac{1}{x_1 x_2},
        \dots,
        0^{m_k - 1}, \tfrac{1}{x_1 \dots x_k};
        1
    }\,,
    \label{eq:LiToG}
    \\
    G\brk{
        0^{m_1 - 1}, x_1,
        \dots,
        0^{m_k - 1}, x_k;
        1
    }
    &=
    \brk{-1}^k \Li_{m_1, \dots, m_k} \brk{
        \tfrac{1}{x_1},
        \tfrac{x_1}{x_2},
        \dots,
        \tfrac{x_{k - 1}}{x_k}
    }\,.
    \label{eq:GtoLi}
\end{align}
%
The most basic and important example of \eqref{eq:seriesLiGen} is the logarithm:
%
\begin{align}
    \Li_1\brk{x} = -G\brk{1; x} = -\log\brk{1 - x} = \sum\limits_{i > 0} \frac{x^i}{i}\,,
    \label{eq:seriesLog}
\end{align}
%
which, together with the rescaling property \eqref{eq:rescaling}, defines
the expansion of a general weight 1 $G$-function $G\brk{z; s} = G\brk{1;
\frac{s}{z}}$.
%
\subsubsection{Behavior near the singular points}
%
More generally, MPLs have at most logarithmic divergences as their endpoint $z$
approaches one of the arguments $a_k$, so, similar to \eqref{eq:logFactorZero},
there always exists an expansion \brk{see also Proposition 3.3.10 in
\cite{Panzer:2015ida}}:
%
\begin{align}
    G\brk{a_1, \ldots, a_k, \ldots, a_n; z}
    = \sum\limits_{i = 0}^N \bigbrk{\log\brk{z - a_k}}^i
    f^{\brk{i}}_{\vv{a}, a_k}\brk{z}\,,
    \label{eq:logFactorGen}
\end{align}
%
such that all $f^{\brk{i}}_{\vv{a}, a_k}\brk{z}$ are analytic near $z = a_k$.
One algorithm for computing such expansions was given in \cite{Panzer:2014caa,
Panzer:2015ida}, but for our application in \autoref{ch:spradlin} we choose the
approach of \cite{Vollinga:2004sn}, which we review in
\autoref{sec:asymptotic}.

This concludes our review of the analytical structure of MPLs. Next we 
turn to a more thorough study of the properties that come with the integral definition
\eqref{eq:defG}, such as shuffle algebra and monodromy. This, in turn, we 
use later in \autoref{ch:spradlin} in the study of scattering amplitude of 7
gluons.
