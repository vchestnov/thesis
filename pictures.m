(* ::Package:: *)

N @ Solve[
	Plus @@ (#^2& /@ ({x, y} - {30, 60})) == 30^2
	/. y -> 40,
	x
]


Module[{eq, sol, pt1, pt2, R = 40},

	pt1 = {100, 80};
	pt2 = {52.3606797749979`,80};
	eq[x_,y_] := Plus @@ (#^2& /@ ({x, y} - {x0, y0})) == R^2;
	sol = First @ Solve[{eq @@ pt1, eq @@ pt2}, {x0, y0}];
	
	StringReplace[
	ToString @ {{x0, y0} /. sol,
	{
		R,
		(ArcTan @@ (pt1 - ({x0, y0} /. sol))) / Pi 180,
		(ArcTan @@ (pt2 - ({x0, y0} /. sol))) / Pi 180
	}},
	{"}, {" -> ")(", "{" -> "(", "}" -> ")"}
	]
]


Module[{eq, sol, pt1, pt2, R = 80},

	pt1 = {140, 80};
	pt2 = {30,90};
	eq[x_,y_] := Plus @@ (#^2& /@ ({x, y} - {x0, y0})) == R^2;
	sol = N @ First @ Solve[{eq @@ pt1, eq @@ pt2}, {x0, y0}];
		
	StringReplace[
	ToString @ {{x0, y0} /. sol,
	{
		R,
		(ArcTan @@ (pt1 - ({x0, y0} /. sol))) / Pi 180,
		(ArcTan @@ (pt2 - ({x0, y0} /. sol))) / Pi 180
	}},
	{"}, {" -> ")(", "{" -> "(", "}" -> ")"}
	]
]


Module[{eq, sol, pt1, pt2, R = 40},

	pt1 = {100, 40};
	pt2 = {52.3606797749979`,40};
	eq[x_,y_] := Plus @@ (#^2& /@ ({x, y} - {x0, y0})) == R^2;
	sol = Last @ Solve[{eq @@ pt1, eq @@ pt2}, {x0, y0}];
	
	StringReplace[
	ToString @ {{x0, y0} /. sol,
	{
		R,
		360 + (ArcTan @@ (pt1 - ({x0, y0} /. sol))) / Pi 180,
		360 + (ArcTan @@ (pt2 - ({x0, y0} /. sol))) / Pi 180
	}},
	{"}, {" -> ")(", "{" -> "(", "}" -> ")"}
	]
]


Module[{eq, sol, pt1, pt2, R = 80},

	pt1 = {140, 40};
	pt2 = {30,30};
	eq[x_,y_] := Plus @@ (#^2& /@ ({x, y} - {x0, y0})) == R^2;
	sol = N @ First @ Solve[{eq @@ pt1, eq @@ pt2}, {x0, y0}];
		
	StringReplace[
	ToString @ {{x0, y0} /. sol,
	{
		R,
		360 + (ArcTan @@ (pt1 - ({x0, y0} /. sol))) / Pi 180,
		360 + (ArcTan @@ (pt2 - ({x0, y0} /. sol))) / Pi 180
	}},
	{"}, {" -> ")(", "{" -> "(", "}" -> ")"}
	]
]


Module[{c, pt1s, pt2s, circ, line, x, y, foo},

c = {40,60};
pt1s = {{0,75}, {0,60}, {0,45}};
pt2s = {{40,70}, {40,60}, {40,50}};

circ := Plus @@ (#^2& /@ ({x, y} - c)) == 20^2;
line[{{x1_, y1_}, {x2_, y2_}}] := (y1 - y2) (x - x1) - (x1 - x2)(y - y1) == 0;

foo = N @ Map[
	Last,
	Solve[{
			circ,
			line @ #
		},
		{x, y}
	],
	{2}
]&;

foo /@ Transpose @ {pt1s, pt2s}
]


Module[{c, pt1s, pt2s, circ, line, x, y, foo},

c = {130,60};
pt1s = {{170,85}, {170,70}, {170,50}, {170,35}};
pt2s = {{130,75}, {130,65}, {130,55}, {130,45}};

circ := Plus @@ (#^2& /@ ({x, y} - c)) == 20^2;
line[{{x1_, y1_}, {x2_, y2_}}] := (y1 - y2) (x - x1) - (x1 - x2)(y - y1) == 0;

foo = N[Map[
	Last,
	Solve[{
			circ,
			line @ #
		},
		{x, y}
	],
	{2}
], 5]&;

StringReplace[ToString[Transpose[foo /@ Transpose @ {pt1s, pt2s}]], {"{" -> "(", "}" -> ")"}]
]
