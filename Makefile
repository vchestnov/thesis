BASENAME = thesis

# From: http://www.acoustics.hut.fi/u/mairas/UltimateLatexMakefile/Makefile
RERUN = "(There were undefined references|Rerun to get (cross-references|the bars) right)"
RERUNBIB = "No file.*\.bbl|Citation.*undefined"
COPY = if test -r $(<:%.tex=%.toc); then cp $(<:%.tex=%.toc) $(<:%.tex=%.toc.bak); fi; if test -r $(<:%.tex=%.lof); then cp $(<:%.tex=%.lof) $(<:%.tex=%.lof.bak); fi; if test -r $(<:%.tex=%.out); then cp $(<:%.tex=%.out) $(<:%.tex=%.out.bak); fi
LATEX = pdflatex -interaction nonstopmode -output-format pdf
BIBTEX	= bibtex
AXOHELP = axohelp
RM = rm -f

define run-latex
	$(COPY); $(LATEX) $< >/dev/null
	egrep -c $(RERUNBIB) $(<:%.tex=%.log) >/dev/null && ($(BIBTEX) $(<:%.tex=%); $(COPY); $(LATEX) $< >/dev/null); true
	egrep $(RERUN) $(<:%.tex=%.log) >/dev/null && ($(COPY); $(LATEX) $< >/dev/null); true
	egrep $(RERUN) $(<:%.tex=%.log) >/dev/null && ($(COPY); $(LATEX) $< >/dev/null); true
	if test -r $(<:%.tex=%.toc); then if cmp -s $(<:%.tex=%.toc) $(<:%.tex=%.toc.bak); then true; else ($(COPY); $(LATEX) $< >/dev/null); fi; fi
	if test -r $(<:%.tex=%.lof); then if cmp -s $(<:%.tex=%.lof) $(<:%.tex=%.lof.bak); then true; else ($(COPY); $(LATEX) $< >/dev/null); fi; fi
	if test -r $(<:%.tex=%.out); then if cmp -s $(<:%.tex=%.out) $(<:%.tex=%.out.bak); then true; else ($(COPY); $(LATEX) $< >/dev/null); fi; fi
	$(RM) $(<:%.tex=%.toc.bak) $(<:%.tex=%.lof.bak) $(<:%.tex=%.out.bak)
	# Display relevant warnings
	egrep "((Reference|Citation).*undefined)|(W| w)arning|(Under|Over)full" $(<:%.tex=%.log); true
	$(AXOHELP) $(BASENAME)
	$(LATEX) $< >/dev/null
endef
# End From: http://www.acoustics.hut.fi/u/mairas/UltimateLatexMakefile/Makefile

# PICS_TIKZ = $(shell ls Fig*.tex)
# PICS_TIKZ_PDF = $(addsuffix .pdf,$(basename $(PICS_TIKZ)))
# PICS_TIKZ_EPS = $(addsuffix .eps,$(basename $(PICS_TIKZ)))

# PICS_MPS = $(shell grep -o 'Fig.*mps' $(BASENAME).mp)
# PICS_MPS_EPS = $(addsuffix .eps,$(basename $(PICS_MPS)))

# PICS_EPS = $(shell ls figures/*eps)
# PICS_EPS_PDF = $(addsuffix .pdf,$(basename $(PICS_EPS)))

PICS_ALL_PDF = $(PICS_TIKZ_PDF) $(PICS_MPS) $(PICS_EPS_PDF)
PICS_ALL_EPS = $(PICS_TIKZ_EPS) $(PICS_MPS_EPS) $(PICS_EPS)

ALLDEPS = $(BASENAME).tex *.tex references.bib nb.bst $(BASENAME).ax1 $(BASENAME).ax2 axo/*
SOURCEFILES = $(ALLDEPS) Makefile $(BASENAME).mp # $(PICS_TIKZ) # $(PICS_EPS)
PDFDEPS = $(ALLDEPS) $(PICS_ALL_PDF)
DVIDEPS = $(ALLDEPS) $(PICS_ALL_EPS)
TARGETFILES = $(BASENAME).pdf $(BASENAME).zip $(BASENAME)_pics.zip $(BASENAME).tar.gz $(BASENAME)_pics.tar.gz $(PICS_ALL_PDF) $(PICS_TIKZ_EPS) $(PICS_MPS_EPS)

# Files that can be safely removed
AUXFILES = '.*\.bbl\|.*\.blg\|.*\.toc\|.*\.lof\|.*\.aux\|.*\.dvi\|.*\.log\|.*\.out\|.*\.ps\|.*\.table\|.*\.gnuplot\|.*\.mps\|.*\.mpx'

.PHONY : pdf
pdf : $(BASENAME).pdf 
$(BASENAME).pdf : $(PDFDEPS)
	@$(run-latex)

.PHONY : ps
ps : $(BASENAME).ps
$(BASENAME).ps : $(BASENAME).dvi
	dvips $<

.PHONY : dvi
dvi : $(BASENAME).dvi
$(BASENAME).dvi : LATEX = pdflatex -interaction nonstopmode -output-format dvi
$(BASENAME).dvi : $(DVIDEPS)
	@$(run-latex)

.PHONY : zip
zip : $(BASENAME).zip
$(BASENAME).zip : $(SOURCEFILES)
	@if [ -e $@ ]; then rm $@; fi
	@zip -9 $@ $^

.PHONY : zippics
zippics : $(BASENAME)_pics.zip
$(BASENAME)_pics.zip : $(PDFDEPS)
	@if [ -e $@ ]; then rm $@; fi
	@zip -9 $@ $^

.PHONY : tar
tar : $(BASENAME).tar.gz
$(BASENAME).tar.gz : $(SOURCEFILES)
	@if [ -e $@ ]; then rm $@; fi
	@tar -czvf $@ $^

.PHONY : tarpics
tarpics : $(BASENAME)_pics.tar.gz
$(BASENAME)_pics.tar.gz : $(PDFDEPS)
	@if [ -e $@ ]; then rm $@; fi
	@tar -czvf $@ $^

$(PICS_TIKZ_PDF) : %.pdf : %.tex
	$(LATEX) --jobname=$(basename $<) $<

$(PICS_TIKZ_EPS) : %.eps : %.tex
	pdflatex -interaction nonstopmode -output-format dvi --jobname=$(basename $<) $<
	dvips $(basename $<).dvi
	ps2eps $(basename $<).ps

$(PICS_EPS_PDF) : %.pdf : %.eps
	@epspdf $<

$(PICS_MPS) : $(BASENAME).mp
	@mpost $< # >/dev/null

$(PICS_MPS_EPS) : %.eps : %.mps
	ln -s $< $@

.PHONY : clean
clean :
	@-for file in $(TARGETFILES); do [ -f $$file ] && rm $$file; done
	@-for file in `find . -regex $(AUXFILES)`; do [ -f $$file ] && rm $$file; done

